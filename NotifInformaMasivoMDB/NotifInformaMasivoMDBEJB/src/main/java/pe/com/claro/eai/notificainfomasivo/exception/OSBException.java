package pe.com.claro.eai.notificainfomasivo.exception;

public class OSBException extends BaseException {
	private static final long serialVersionUID = 1L;
	private String nombreOSB;
	private String nombreMetodo;
	private String nombreUrl;

	public OSBException(String codError, String nombreOSB, String nombreUrl, String msjError, Exception objException) {
		super(codError, msjError, objException);
		this.nombreOSB = nombreOSB;
		this.nombreMetodo = nombreUrl;
	}

	public OSBException(String codError, String nombreOSB, String nombreMetodo, String msjError,
			Throwable objException) {
		super(codError, msjError, objException);
		this.nombreOSB = nombreOSB;
		this.nombreMetodo = nombreMetodo;
	}

	public OSBException(String msjError, Exception objException) {
		super(msjError, objException);
	}

	public OSBException(Exception objException) {
		super(objException);
	}

	public OSBException(String msjError) {
		super(msjError);
	}

	public String getNombreOSB() {
		return this.nombreOSB;
	}

	public void setNombreOSB(String nombreOSB) {
		this.nombreOSB = nombreOSB;
	}

	public String getNombreMetodo() {
		return this.nombreMetodo;
	}

	public void setNombreMetodo(String nombreMetodo) {
		this.nombreMetodo = nombreMetodo;
	}

	public String getNombreUrl() {
		return nombreUrl;
	}

	public void setNombreUrl(String nombreUrl) {
		this.nombreUrl = nombreUrl;
	}

}
