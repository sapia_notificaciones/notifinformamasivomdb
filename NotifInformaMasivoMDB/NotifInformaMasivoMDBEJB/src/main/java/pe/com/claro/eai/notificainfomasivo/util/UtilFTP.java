package pe.com.claro.eai.notificainfomasivo.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.ChannelSftp;
import org.apache.commons.vfs.FileSystemOptions;
import org.apache.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.vfs.FileSystemException;
import org.apache.commons.vfs.provider.sftp.SftpClientFactory;
import org.apache.commons.vfs.provider.sftp.SftpFileSystemConfigBuilder;
import com.jcraft.jsch.Channel;

import pe.com.claro.eai.notificainfomasivo.dao.NotifDBDAO;
import pe.com.claro.eai.notificainfomasivo.exception.SFTPException;

import com.jcraft.jsch.ChannelExec;

@Component
public class UtilFTP {
	@Autowired
	private PropertiesExterno externalProperties;

	@Autowired
	private NotifDBDAO notifDB;
	
	private Logger logger = Logger.getLogger(this.getClass().getName());
	private ChannelSftp ojbCanalSFTP = null;
	private Session objSesion = null;

	/**
	 * conectarFTP
	 * 
	 * @param trazabilidadParam
	 * @param vServidorSFTP
	 * @param vUsuarioSFTP
	 * @param vPasswordSFTP
	 * @param vPuertoSFTP
	 * @param vTimeOutSFTP
	 * @return boolean
	 * @throws SFTPException
	 */
	public boolean conectarSFTP(String trazabilidadParam, String servidorSFTP, String usuarioSFTP, String passwordSFTP,
			int puertoSFTP, int timeOutSFTP) throws SFTPException {

		String trazabilidad = trazabilidadParam + " [conectarSFTP]";
		logger.info(trazabilidad + " [INICIO] - METODO: [conectarSFTP] ");

		FileSystemOptions objFileSystemOptions = null;
		Channel objCanal = null;
		boolean estadoConexion = false;

		try {

			if (this.ojbCanalSFTP != null) {
				desconectarSFTP(trazabilidad);
			}

			objFileSystemOptions = new FileSystemOptions();

			SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(objFileSystemOptions,
					Constantes.NO_MINUSCULA);

			objSesion = SftpClientFactory.createConnection(servidorSFTP, puertoSFTP, usuarioSFTP.toCharArray(),
					passwordSFTP.toCharArray(), objFileSystemOptions);
			objCanal = this.objSesion.openChannel(externalProperties.cCARGA_SFTP_TIPO_CANAL_SESION);
			objCanal.connect(timeOutSFTP);

			ojbCanalSFTP = (ChannelSftp) objCanal;
			estadoConexion = this.ojbCanalSFTP.isConnected();
		} catch (FileSystemException e) {
			logger.error(trazabilidad + " ERROR [FileSystemException]: ", e);
			estadoConexion = false;
						
			throw new SFTPException(Constantes.CADENA_VACIA, Constantes.ERROR_DETALLE + e.getMessage());
		} catch (Exception e) {
			logger.error(trazabilidad + Constantes.ERROR_DETALLE, e);
			estadoConexion = false;
			throw new SFTPException(Constantes.CADENA_VACIA, Constantes.ERROR_DETALLE + e.getMessage());
		} finally {
			logger.info(trazabilidad + " [FIN] - METODO: [conectarSFTP] ");
		}

		return estadoConexion;
	}

	/**
	 * desconectarSFTP
	 * 
	 * @param trazabilidadParam
	 * @return boolean
	 */
	public boolean desconectarSFTP(String trazabilidadParam) {

		String trazabilidad = trazabilidadParam + "[desconectarSFTP]";
		this.logger.info(trazabilidad + " [INICIO] - METODO: [desconectarSFTP ] ");

		boolean estadoConexion = false;

		try {
			if (this.ojbCanalSFTP != null) {
				this.ojbCanalSFTP.exit();
			}

			if (this.objSesion != null) {
				this.objSesion.disconnect();
			}

			this.ojbCanalSFTP = null;
			estadoConexion = true;
		} catch (Exception e) {
			this.logger.error(trazabilidad + Constantes.ERROR_DETALLE, e);
			estadoConexion = false;
		} finally {
			this.logger.info(trazabilidad + " [FIN] - METODO: [desconectarSFTP ] ");
		}

		return estadoConexion;
	}

	public long obtenerNumeroRegistrosFTP(String trazabilidadParam, String idTransaccion,String rutaFichero, String nombreFichero)
			throws SFTPException, IOException {

		boolean estadoConexion = false;
		
		try {

			estadoConexion = this.objSesion.isConnected();

			
			if(!estadoConexion) {
				
				conectarSFTP(trazabilidadParam, externalProperties.cCARGA_SFTP_IP,
						externalProperties.cCARGA_SFTP_USUARIO, externalProperties.cCARGA_SFTP_CONTRASENA,
						externalProperties.cCARGA_SFTP_PUERTO, externalProperties.cCARGA_SFTP_TIMEOUT);
			}
			
			this.logger.info(trazabilidadParam + " [INICIO] - METODO: [obtenerNumeroRegistrosFTP ] ");

			Channel channel = objSesion.openChannel("exec");
			
			String comando = Constantes.COMANDO_CD+Constantes.TEXTO_VACIO+rutaFichero + Constantes.TEXTO_VACIO + Constantes.VALOR_PUNTOCOMA
					+ Constantes.TEXTO_VACIO + "wc -l" + Constantes.TEXTO_VACIO + nombreFichero;
						
			logger.info("Se ejecutara el comando: " + comando);
			
			((ChannelExec) channel).setCommand(comando);
			channel.setInputStream(null);

			InputStream input = channel.getInputStream();
			channel.connect();

			try {

				InputStreamReader inputReader = new InputStreamReader(input);
				BufferedReader bufferedReader = new BufferedReader(inputReader);

				String line;
				while ((line = bufferedReader.readLine()) != null) {

					String[] valores = line.split(Constantes.TEXTO_ESPACIO_BLANCO);
					return Long.parseLong(valores[0]);
				}

			} catch (Exception e) {
				this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
			}

		} catch (Exception e) {
			this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
			notifDB.registrarError(trazabilidadParam, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_SFTP, e.toString());
			return 0;
		} finally {
			this.logger.info(trazabilidadParam + " [FIN] - METODO: [obtenerNumeroRegistrosFTP ] ");
		}
		return 0;

	}

	public long obtenerTamanioFicheroFTP(String trazabilidadParam,String idTransaccion, String rutaFichero, String nombreFichero)
			throws SFTPException, IOException {

		boolean estadoConexion = false;
		
		try {

			estadoConexion = this.objSesion.isConnected();
			
			if(!estadoConexion) {
				
				conectarSFTP(trazabilidadParam, externalProperties.cCARGA_SFTP_IP,
						externalProperties.cCARGA_SFTP_USUARIO, externalProperties.cCARGA_SFTP_CONTRASENA,
						externalProperties.cCARGA_SFTP_PUERTO, externalProperties.cCARGA_SFTP_TIMEOUT);
			}
			
			this.logger.info(trazabilidadParam + " [INICIO] - METODO: [obtenerTamanioFicheroFTP ] ");
			
			

			Channel channel = objSesion.openChannel("exec");
		
			String comando = Constantes.COMANDO_CD + Constantes.TEXTO_VACIO + rutaFichero + Constantes.TEXTO_VACIO + Constantes.VALOR_PUNTOCOMA
					+ Constantes.TEXTO_VACIO + "" + Constantes.COMANDO_STAT + " -c %s" + Constantes.TEXTO_VACIO
					+ nombreFichero;
			
			logger.info("Se ejecutara el comando: " + comando);

			((ChannelExec) channel).setCommand(comando);
			channel.setInputStream(null);

			InputStream input = channel.getInputStream();
			channel.connect();

			try {

				InputStreamReader inputReader = new InputStreamReader(input);
				BufferedReader bufferedReader = new BufferedReader(inputReader);

				String line;
				while ((line = bufferedReader.readLine()) != null) {
					return Long.parseLong(line);
				}

			} catch (Exception e) {
				this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
			}

		} catch (Exception e) {
			this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
			notifDB.registrarError(trazabilidadParam, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_SFTP, e.toString());
		} finally {
			this.logger.info(trazabilidadParam + " [FIN] - METODO: [obtenerTamanioFicheroFTP ] ");
		}

		return 0;

	}
	
	public long eliminarFicheroSFTP(String trazabilidadParam,String idTransaccion, String rutaFichero)
			throws SFTPException, IOException {

		boolean estadoConexion = false;
		
		try {

			estadoConexion = this.objSesion.isConnected();
			
			if(!estadoConexion) {
				
				conectarSFTP(trazabilidadParam, externalProperties.cCARGA_SFTP_IP,
						externalProperties.cCARGA_SFTP_USUARIO, externalProperties.cCARGA_SFTP_CONTRASENA,
						externalProperties.cCARGA_SFTP_PUERTO, externalProperties.cCARGA_SFTP_TIMEOUT);
			}
			
			this.logger.info(trazabilidadParam + " [INICIO] - METODO: [obtenerTamanioFicheroFTP ] ");
			
			Channel channel = objSesion.openChannel("exec");
		
			String comando = Constantes.COMANDO_CD + Constantes.TEXTO_VACIO + rutaFichero + Constantes.TEXTO_VACIO + Constantes.VALOR_PUNTOCOMA	+ 
							 Constantes.TEXTO_VACIO + "" + Constantes.COMANDO_RM + Constantes.TEXTO_VACIO + "*";
			
			logger.info("Se ejecutara el comando: " + comando);

			((ChannelExec) channel).setCommand(comando);
			channel.setInputStream(null);

			channel.connect();


		} catch (Exception e) {
			this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
			notifDB.registrarError(trazabilidadParam, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_SFTP, e.toString());

		} finally {
			this.logger.info(trazabilidadParam + " [FIN] - METODO: [obtenerTamanioFicheroFTP ] ");
		}

		return 0;

	}

	public List<String> particionarFicheroFTP(String trazabilidadParam,String idTransaccion, String rutaFichero, String nombreFichero,
			String rutaNuevaFichero,String nombreFicheroRenombrado, long numeroRegistros) throws SFTPException, IOException {

		List<String> filesNames = new ArrayList<>();

		boolean estadoConexion = false;
		
		try {

			estadoConexion = this.objSesion.isConnected();
			
			if(!estadoConexion) {
				
				conectarSFTP(trazabilidadParam, externalProperties.cCARGA_SFTP_IP,
						externalProperties.cCARGA_SFTP_USUARIO, externalProperties.cCARGA_SFTP_CONTRASENA,
						externalProperties.cCARGA_SFTP_PUERTO, externalProperties.cCARGA_SFTP_TIMEOUT);
			}
			
			this.logger.info(trazabilidadParam + " [INICIO] - METODO: [particionarFicheroFTP ] ");
			
			Channel channel = objSesion.openChannel("exec");
			
			StringBuilder comandodividirArchivo = new StringBuilder();
			comandodividirArchivo.append(Constantes.COMANDO_CD).append(Constantes.TEXTO_VACIO).append(rutaFichero)
					.append(Constantes.VALOR_PUNTOCOMA).append("awk 'NR%").append(Constantes.COMILLA_DOBLE)
					.append(Constantes.COMILLA_SIMPLE).append(Constantes.COMILLA_DOBLE).append(numeroRegistros)
					.append(Constantes.COMILLA_DOBLE).append(Constantes.COMILLA_SIMPLE).append(Constantes.COMILLA_DOBLE)
					.append("==1{").append("x=").append(Constantes.COMILLA_DOBLE).append(Constantes.COMILLA_SIMPLE)
					.append(Constantes.COMILLA_DOBLE).append(externalProperties.cCARGA_SFTP_POR_PROCESAR+Constantes.CARACTER_SLASH+nombreFicheroRenombrado + "_")
					.append(Constantes.COMILLA_DOBLE).append(Constantes.COMILLA_SIMPLE).append(Constantes.COMILLA_DOBLE)
					.append(Constantes.TEXTO_VACIO).append("sprintf(").append(Constantes.COMILLA_DOBLE).append("%01d")
					.append(Constantes.COMILLA_DOBLE).append(Constantes.COMA).append("++i)")
					.append(Constantes.TEXTO_VACIO).append(Constantes.COMILLA_DOBLE).append(".txt")
					.append(Constantes.COMILLA_DOBLE).append("}{print > x}").append(Constantes.COMILLA_SIMPLE)
					.append(Constantes.TEXTO_VACIO).append(nombreFichero).append(Constantes.VALOR_PUNTOCOMA)
					.append(Constantes.COMANDO_CD).append(Constantes.TEXTO_VACIO).append(rutaNuevaFichero)
					.append(Constantes.VALOR_PUNTOCOMA)
					.append(Constantes.COMANDO_LS).append(Constantes.TEXTO_VACIO).append("-1")
					.append(Constantes.TEXTO_VACIO).append(nombreFicheroRenombrado + Constantes.GUION_BAJO).append("*");
					
			logger.info("Se ejecutara el comando: " + comandodividirArchivo.toString());
			
			((ChannelExec) channel).setCommand(comandodividirArchivo.toString());
			channel.setInputStream(null);

			InputStream input = channel.getInputStream();
			channel.connect();

			try {

				InputStreamReader inputReader = new InputStreamReader(input);
				BufferedReader bufferedReader = new BufferedReader(inputReader);

				String line;
				while ((line = bufferedReader.readLine()) != null) {

					if (!line.equals(Constantes.VACIO)) {

						filesNames.add(line);
					}

					this.logger.info(trazabilidadParam + " Se particiono el fichero a:" + line);

				}

			} catch (Exception e) {
				this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
			}

		} catch (Exception e) {

			this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
			notifDB.registrarError(trazabilidadParam, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_SFTP, e.toString());

		} finally {
			this.logger.info(trazabilidadParam + " [FIN] - METODO: [particionarFicheroFTP ] ");
		}

		return filesNames;

	}

	
	public String generarFicheroPorColumnas(String trazabilidadParam,String idTransaccion, String rutaFichero,String nombreFichero, String nombreFicheroSalida,
			String msje,int nroFicheroMsje) throws SFTPException{

		

		this.logger.info(trazabilidadParam + " [INICIO] - METODO: [generarFicheroPorColumnas ] ");
		
		boolean estadoConexion = false;
		String fileGenerado = "";
		
		
		try {
			
			estadoConexion = this.objSesion.isConnected();
							
			if(!estadoConexion) {
				
				conectarSFTP(trazabilidadParam, externalProperties.cCARGA_SFTP_IP,
						externalProperties.cCARGA_SFTP_USUARIO, externalProperties.cCARGA_SFTP_CONTRASENA,
						externalProperties.cCARGA_SFTP_PUERTO, externalProperties.cCARGA_SFTP_TIMEOUT);
			}
			
			Channel channel = objSesion.openChannel("exec");
			
			String nombreFile = nombreFicheroSalida.substring(0, nombreFicheroSalida.length() - 4)+Constantes.GUION_BAJO+nroFicheroMsje+Constantes.FORMATO_TXT;
			
			String comando = Constantes.COMANDO_CD+ Constantes.TEXTO_VACIO + rutaFichero + Constantes.VALOR_PUNTOCOMA +Constantes.COMANDO_AWK +" 'BEGIN{FS=\""+Constantes.VALOR_PALOTE+"\"} {print "+msje+"}' " + nombreFichero + " > " + 
							nombreFile+Constantes.VALOR_PUNTOCOMA+
							Constantes.COMANDO_LS + Constantes.TEXTO_VACIO + "-1" + Constantes.TEXTO_VACIO +nombreFile +"*";
			
			logger.info("Se ejecutara el comando: " + comando);
			
			((ChannelExec) channel).setCommand(comando);

			channel.setInputStream(null);

			InputStream input = channel.getInputStream();
			channel.connect();

			try {

				InputStreamReader inputReader = new InputStreamReader(input);
				BufferedReader bufferedReader = new BufferedReader(inputReader);

				String line;
				while ((line = bufferedReader.readLine()) != null) {

					if (!line.equals(Constantes.VACIO)) {

						fileGenerado = line;
						logger.info("Se genero el archivo " + line);
					}

					this.logger.info(trazabilidadParam + " Se particiono el fichero a:" + line);

				}

			} catch (Exception e) {
				this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
				notifDB.registrarError(trazabilidadParam, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_SFTP, e.toString());

			}


		} catch (Exception e) {
			this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
		}finally {
			
			this.logger.info(trazabilidadParam + " [FIN] - METODO: [generarFicheroPorColumnas ] ");
			
		}
		
		return fileGenerado;

	}

	public boolean validarPermisosCarpetas(String trazabilidadParam,String idTransaccion, String rutaBase, List<String> listaCarpetasFiltrar,
			String permiso, String usuario) throws SFTPException, IOException {

		List<String> listaFicherosFTP = new ArrayList<String>();

		boolean retorno = false;
		boolean estadoConexion = false;
		
		try {

			estadoConexion = this.objSesion.isConnected();
			
			if(estadoConexion==false) {
				
				conectarSFTP(trazabilidadParam, externalProperties.cCARGA_SFTP_IP,
						externalProperties.cCARGA_SFTP_USUARIO, externalProperties.cCARGA_SFTP_CONTRASENA,
						externalProperties.cCARGA_SFTP_PUERTO, externalProperties.cCARGA_SFTP_TIMEOUT);
			}

							
			this.logger.info(trazabilidadParam + " [INICIO] - METODO: [validarPermisosCarpetas ] ");
			
			Channel channel = objSesion.openChannel("exec");
			String comando = Constantes.COMANDO_CD + Constantes.TEXTO_VACIO + rutaBase + Constantes.TEXTO_VACIO
					+ Constantes.VALOR_PUNTOCOMA + Constantes.TEXTO_VACIO + "" + Constantes.COMANDO_LS
					+ Constantes.TEXTO_VACIO + "-C -" + Constantes.COMANDO_ALL + Constantes.VALOR_PALOTE
					+ Constantes.TEXTO_VACIO + Constantes.COMANDO_AWK + " '" + "{" + "if(($1=="
					+ Constantes.COMILLA_DOBLE + permiso + Constantes.COMILLA_DOBLE + ") && ($3=="
					+ Constantes.COMILLA_DOBLE + usuario + Constantes.COMILLA_DOBLE + "))" + Constantes.TEXTO_VACIO
					+ Constantes.COMANDO_PRINT + Constantes.TEXTO_VACIO + "$1 " + Constantes.SEPARADOR_COMILLAS
					+ Constantes.VALOR_PUNTOCOMA + Constantes.SEPARADOR_COMILLAS + " $3 "
					+ Constantes.SEPARADOR_COMILLAS + Constantes.VALOR_PUNTOCOMA + Constantes.SEPARADOR_COMILLAS
					+ " $9 }'";

			logger.info("Se ejecutara el comando: " + comando);
			
			((ChannelExec) channel).setCommand(comando);
			channel.setInputStream(null);

			InputStream input = channel.getInputStream();
			channel.connect();

			try {

				InputStreamReader inputReader = new InputStreamReader(input);
				BufferedReader bufferedReader = new BufferedReader(inputReader);

				String line;
				long contador = 0;
				while ((line = bufferedReader.readLine()) != null) {

					String[] valores = line.split(Constantes.VALOR_PUNTOCOMA);

					listaFicherosFTP.add(valores[2]);

					for (String ficheroFiltrar : listaCarpetasFiltrar) {

						if (ficheroFiltrar.equals(valores[2])) {
							contador++;
						}

					}

				}

				if (contador == listaCarpetasFiltrar.size()) {
					retorno = true;

					this.logger.info(trazabilidadParam + "Existen permisos para las carpetas");

				} else {
					retorno = false;

					this.logger.info(trazabilidadParam + "No Existen permisos para las carpetas");
				}

			} catch (Exception e) {
				this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
				notifDB.registrarError(trazabilidadParam, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_SFTP, e.toString());
			}

		} catch (Exception e) {
			this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
		} finally {
			this.logger.info(trazabilidadParam + " [FIN] - METODO: [validarPermisosCarpetas ] ");
		}

		return retorno;

	}

	public ArrayList<String> leerArchivoFTP(String trazabilidadParam,String idTransaccion,String rutaFichero,
			String nombreFichero) throws SFTPException, IOException {

		ArrayList<String> listLineas = new ArrayList<>();
		boolean estadoConexion = false;
		Channel channel = null;
		
		try {

			this.logger.info(trazabilidadParam + " [INICIO] - METODO: [leerArchivoFTP ] ");

			channel = this.objSesion.openChannel(externalProperties.cCARGA_SFTP_TIPO_CANAL_SESION);
			ojbCanalSFTP = (ChannelSftp) channel;
			estadoConexion = this.ojbCanalSFTP.isConnected();
			
			if(estadoConexion==false) {
				
				conectarSFTP(trazabilidadParam, externalProperties.cCARGA_SFTP_IP,
						externalProperties.cCARGA_SFTP_USUARIO, externalProperties.cCARGA_SFTP_CONTRASENA,
						externalProperties.cCARGA_SFTP_PUERTO, externalProperties.cCARGA_SFTP_TIMEOUT);
			}
			
			channel = objSesion.openChannel("exec");
			String comando = Constantes.COMANDO_CD + Constantes.TEXTO_VACIO + rutaFichero + Constantes.TEXTO_VACIO
					+ Constantes.VALOR_PUNTOCOMA + Constantes.TEXTO_VACIO + "" + Constantes.COMANDO_CAT
					+ Constantes.TEXTO_VACIO + nombreFichero;
			
			logger.info("Se ejecutara el comando: " + comando);
			
			((ChannelExec) channel).setCommand(comando);
			channel.setInputStream(null);

			InputStream input = channel.getInputStream();
			channel.connect();

			try {

				InputStreamReader inputReader = new InputStreamReader(input);
				BufferedReader bufferedReader = new BufferedReader(inputReader);

				String lineas;
				while ((lineas = bufferedReader.readLine()) != null) {
					
					listLineas.add(lineas);
					
					
				}

			} catch (Exception e) {
				this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
			}

		} catch (Exception e) {
			this.logger.error(trazabilidadParam + Constantes.ERROR_DETALLE, e);
			notifDB.registrarError(trazabilidadParam, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_SFTP, e.toString());

		} finally {
			this.logger.info(trazabilidadParam + " [FIN] - METODO: [leerArchivoFTP ] ");
		}

		return listLineas;

	}

	

}