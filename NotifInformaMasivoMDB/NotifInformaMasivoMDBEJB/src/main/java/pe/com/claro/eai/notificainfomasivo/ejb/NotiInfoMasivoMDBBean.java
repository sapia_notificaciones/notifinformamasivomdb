package pe.com.claro.eai.notificainfomasivo.ejb;

import java.util.UUID;

import javax.ejb.MessageDriven;
import javax.interceptor.Interceptors;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import pe.com.claro.eai.notificainfomasivo.service.NotiInfoMasivoMDBService;
import pe.com.claro.eai.notificainfomasivo.util.Constantes;
import pe.com.claro.eai.notificainfomasivo.util.SpringBeanInterceptor;

@MessageDriven
@Interceptors(SpringBeanInterceptor.class)
public class NotiInfoMasivoMDBBean implements MessageListener {
	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	@Autowired
	private NotiInfoMasivoMDBService notificacionInformativaMasivoMDBService;

	public NotiInfoMasivoMDBBean() {
		logger.info("*** 'EJB' [NotiInfoMasivoMDBBean] - Inicializado ... ***");
	}


	@Override
	public void onMessage(Message message) {
		final long vTiempoProceso = System.currentTimeMillis();
		String mensajeTransversal = "[UUID:" + UUID.randomUUID().getMostSignificantBits() + "]";
		logger.info(mensajeTransversal + "[INICIO][METODO:onMessage]");
		try {
			if (message instanceof TextMessage) {
				TextMessage txtMessage = (TextMessage) message;
				String strMensajeXML = txtMessage.getText();
				logger.info(mensajeTransversal + "[TRAMA ]:" + strMensajeXML);
				
				String[] valores= strMensajeXML.split(Constantes.VALOR_PALOTE_UNIC);
				String idNotificacion = valores[Constantes.VALOR_CERO_ENTERO];
				String fichero = valores[Constantes.VALOR_UNO_ENTERO];
				
				notificacionInformativaMasivoMDBService.procesar(mensajeTransversal, idNotificacion, fichero);
				 
				 
			} else {
				logger.error("El mensaje no es de tipo TextMessage.");
			}
		} catch (JMSException e) {
			logger.error(mensajeTransversal + "[JMSException][Error:" + e.getMessage() + "]", e);
		} catch (Exception e) {
			logger.error(mensajeTransversal + "[Exception][Error:" + e.getMessage() + "]", e);
		} catch (Throwable e) {
			logger.error(mensajeTransversal + "[Throwable][Error:" + e.getMessage() + "]", e);
		} finally {
			logger.info(mensajeTransversal + " - Tiempo total de proceso(ms): "
					+ (System.currentTimeMillis() - vTiempoProceso) + " milisegundos.");
			logger.info(mensajeTransversal + "[FIN][METODO:onMessage]");
		}
	}
}