package pe.com.claro.eai.notificainfomasivo.proxy;



import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import pe.com.claro.eai.notificainfomasivo.bean.createCampaingApp.request.CreateCampaignAppRequest;
import pe.com.claro.eai.notificainfomasivo.bean.createCampaingApp.request.response.CreateCampaignAppResponse;
import pe.com.claro.eai.notificainfomasivo.exception.WSException;
import pe.com.claro.eai.notificainfomasivo.util.Constantes;
import pe.com.claro.eai.notificainfomasivo.util.PropertiesExterno;


@Service
public class EnviaAppClienteImpl implements EnviaAppCliente{

	private static Logger logger = Logger.getLogger(EnviaAppClienteImpl.class);
	
	@Autowired
	private PropertiesExterno constantesexternos;
	

	
	@Override
	public CreateCampaignAppResponse enviarApp(String idTransaccion,String token,String mensajeTransaccion,
			CreateCampaignAppRequest request) throws WSException {
		
		String mensajeMetodo = mensajeTransaccion + "[enviaApp] ";
		
		CreateCampaignAppResponse enviaAppResponse = new CreateCampaignAppResponse();
		
		String url = "https://fcmk06ioha.execute-api.us-east-1.amazonaws.com/prd/push/campania";
		
		try {
			
			Gson inputGson = new Gson();			
			String inputJson = inputGson.toJson(request, CreateCampaignAppRequest.class);

			logger.info(mensajeMetodo + "JSON: " + inputJson);		
			
			Client client = Client.create();
			
			client.setConnectTimeout(Integer.parseInt("50000"));
			client.setReadTimeout(Integer.parseInt("50000"));
			
			WebResource webResource = client.resource(url);
			
			ClientResponse response = null;
			
			
			response = webResource.accept(Constantes.VALUEAPPLICATIONJSON)
						.header(Constantes.NAMEHEADERAPPCONTENTTYPE, "application/json")
						.header("request-date", "2018-03-26T09:09:09.000Z")
						.header("request-id", "1573074586")
						.header("client-id", "1573074586")						
						.post(ClientResponse.class, inputJson);
			
			
			if(response!=null){
				
				logger.info( mensajeMetodo + "Status: " + response.getStatus());
				
				String rpta = response.getEntity(String.class);
				logger.info( mensajeMetodo + "Parametros Salida:" + rpta);
				
				if(response.getStatus() == 200){
					enviaAppResponse = new CreateCampaignAppResponse();
					Gson gson = new Gson();
					enviaAppResponse = gson.fromJson(rpta, CreateCampaignAppResponse.class);				
				}else{
					logger.info( mensajeMetodo + "Error en el Servicio WS EnviaApp Error:" +   response.getStatus());
					
				}	
				
			}
			
			
		} catch (Exception e) {
			logger.error( mensajeMetodo + "Excepcion ocurrida en el WS [" + "WS EnviaApp"+ "]: [" + e.getMessage() + "]", e );

				
		}finally{
			logger.info( mensajeMetodo + "[FIN] - METODO: [EnviaApp - WS] ");
		}
		
		return enviaAppResponse;
	}




}
