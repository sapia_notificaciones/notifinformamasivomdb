package pe.com.claro.eai.notificainfomasivo.util;

public class Constantes {

	private Constantes() {
		super();
	}

	public static final String TEXTO_VACIO=" ";
	public static final String TEXTO_ESPACIO_BLANCO=" ";
	public static final String PUNTO=".";
	
	public static final String NO_MINUSCULA="no";
	public static final String VACIO = "";

	public static final String SALTO_LINEA = "\n";
	public static final String COMA = ",";
	public static final String CARACTER_INTERROGANTE = "?";
	public static final String GUION_BAJO = "_";
	public static final String FORMATO_TXT = ".txt";
	
	
	
	public static final String SEPARADOR_COMILLAS = "\"";
	public static final String SEPARADOR_BACKSLASH_UNICODE = "\u005C\";
	
	public static final String VALOR_PUNTO = ".";
	public static final String CADENA_VACIA = "";
	public static final String ERROR_DETALLE = "Detalle Error:";
	public static final String VALOR_PUNTOCOMA = ";";
	
	public static final String VALOR_PALOTE = "|";
	public static final String VALOR_PALOTE_UNIC = "\\|";
	public static final String CARACTER_IGUAL = "=";
	
	public static final String CAMPO_NROTELEFONICO = "nroTelefonico";
	
	public static final String COMANDO_CD ="cd";
	public static final String COMANDO_PRINT ="print";
	public static final String COMANDO_AWK ="awk";
	public static final String COMANDO_BEGIN= "BEGIN";
	public static final String COMANDO_FS= "FS";
	public static final String COMANDO_SPLIT="split";
	public static final String COMANDO_STAT ="stat";
	public static final String COMANDO_LS ="ls";
	public static final String COMANDO_ALL ="all";
	public static final String COMANDO_CAT ="cat";
	public static final String COMANDO_RM ="rm";
	public static final String COMPONENTE_MDB_NOTIF_MASIVO="NotifInformaMasivoMDB";
	
	public static final String CODIGO_EXITO = "0";
	
	public static final String CODIGO_ERROR_HIST ="0";
	public static final String CODIGO_EXITO_HIST ="1";
	
	public static final String NAMEHEADERAPPCONTENTTYPE="content-type";
	public static final String NAMEHEADERAPPACCESSTOKEN="access_token";
	public static final String NAMEHEADERAPPIDTRANSACCION="idTransaccion";
	public static final String NAMEHEADERAPPTIMESTAMP="timestamp";
	public static final String NAMEHEADERAPPAUTHORIZATION="Authorization";
	public static final String VALUEACCEPTTYPE="content-type";
	public static final String VALUEAPPLICATIONJSON="application/json";

	//JFlores
	public static final String TABLA_NOTIFICA_REGISTRO="NOTIFICA.CFG_NOTIFINFO_HIST_MASIVO";
	public static final String CAMPO_IDNOTIFICACION="TCFGV_IDNOTIFICACION";
	public static final String CAMPO_IDENTIFICADOR="TCFGV_IDENTIFICADOR";
	public static final String CAMPO_LOTE="TCFGV_LOTE";
	public static final String CAMPO_ESTADO="TCFGV_ESTADO";
	public static final String CAMPO_FECHA="TCFGD_FECHA";
	public static final String CAMPO_CANAL="TCFGV_CANAL";
	public static final String CAMPO_TIPO="TCFGV_TIPO";
	public static final String VALOR_CERO="0";
	public static final String MENSAJE_OK="OK";
	public static final int VALOR_CUATRO=4;
	public static final int VALOR_TRES=3;
	
	public static final int VALOR_CERO_ENTERO=0;
	public static final int VALOR_UNO_ENTERO=1;
	
	public static final String MENSAJE_CONECTADO="Conectado";
	public static final String MENSAJE_CONEXION_ERRONEA="Conexion erronea";
	
	public static final String AWK_SPLIT_LINUX="cd rutaArchivo; awk 'NR%\"'\"cantPartir\"'\"==1{x=\"'\"MSJ_SMS_\"'\" sprintf(\"%01d\",++i) \".txt\"}{print > x}' MSJ_SMS_1.txt";
	public static final String UTF_8 ="UTF-8";
	
	public static final String COMILLA = "\"";
	public static final String COMILLA_SIMPLE = "'";
	public static final String COMILLA_DOBLE = "\"";
	public static final String LLAVE_IZQUIERDA = "{";
	public static final String LLAVE_DERECHA = "}";
	
	public static final String SEPARADOR_COMA = ",";
	public static final String CARACTER_SLASH = "/";
	public static final String CARACTER_DOBLESLASH = "//";
	public static final String CARACTER_ARROBA = "@";
	public static final String SIGNO_DOS_PUNTOS = ":";

	public static final String TIMEZONEGMT="GMT";
	
	
	
	public static final String MSJE_INICIOMETODO="[INICIO] - METODO: [metodo - UTILITARIOS] ";
	public static final String METODO="metodo";
	
	
	////ERRORES////
	
	public static final String CODIGO_ERROR_EXCEPTION_GENERAL="-1";
	public static final String CODIGO_ERROR_GESTOR_SMS="-2";
	public static final String CODIGO_ERROR_CONECTIVIDAD="-3";
	public static final String CODIGO_ERROR_SINREGISTROS="-4";
	public static final String CODIGO_ERROR_BD="-5";
	public static final String CODIGO_ERROR_SFTP="-6";
	public static final String MSJE_ERROR_SINREGISTROS="No existen registros a procesar";
	public static final String MSJE_ERROR_CONECTIVIDAD="Error de conectividad";
	
	
	
}
