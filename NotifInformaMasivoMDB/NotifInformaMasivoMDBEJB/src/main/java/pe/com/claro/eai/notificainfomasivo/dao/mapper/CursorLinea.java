package pe.com.claro.eai.notificainfomasivo.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.eai.notificainfomasivo.bean.Clinea;

public class CursorLinea implements RowMapper<Clinea>  {

	@Override
	public Clinea mapRow(ResultSet rs, int numeroFila) throws SQLException {
		
		Clinea clinea = new Clinea();
		clinea.setIdNotificacion(rs.getString("IDNOTIFICACION"));
		clinea.setIdentificador(rs.getString("USAPV_IDENTIFICADOR"));
		return clinea;
	}

}
