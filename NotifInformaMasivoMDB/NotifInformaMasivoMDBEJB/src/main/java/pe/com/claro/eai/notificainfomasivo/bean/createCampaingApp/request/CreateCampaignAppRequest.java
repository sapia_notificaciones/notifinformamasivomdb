package pe.com.claro.eai.notificainfomasivo.bean.createCampaingApp.request;

public class CreateCampaignAppRequest {

	private Canal canal;
	private Categoria categoria;
	private String nombre;
	private Integer tipo;
	private Plantilla plantilla;
	private Integer tps;
	private Integer vigencia;
	private Accion accion;
	private String urlArchivo;
	private String urlPush;
	private String urlBandeja;
	private Usuario usuario;
	private Data data;
	public Canal getCanal() {
		return canal;
	}
	public void setCanal(Canal canal) {
		this.canal = canal;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public Plantilla getPlantilla() {
		return plantilla;
	}
	public void setPlantilla(Plantilla plantilla) {
		this.plantilla = plantilla;
	}
	public Integer getTps() {
		return tps;
	}
	public void setTps(Integer tps) {
		this.tps = tps;
	}
	public Integer getVigencia() {
		return vigencia;
	}
	public void setVigencia(Integer vigencia) {
		this.vigencia = vigencia;
	}
	public Accion getAccion() {
		return accion;
	}
	public void setAccion(Accion accion) {
		this.accion = accion;
	}
	public String getUrlArchivo() {
		return urlArchivo;
	}
	public void setUrlArchivo(String urlArchivo) {
		this.urlArchivo = urlArchivo;
	}
	public String getUrlPush() {
		return urlPush;
	}
	public void setUrlPush(String urlPush) {
		this.urlPush = urlPush;
	}
	public String getUrlBandeja() {
		return urlBandeja;
	}
	public void setUrlBandeja(String urlBandeja) {
		this.urlBandeja = urlBandeja;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	
	
	
	
	
	
	
}
