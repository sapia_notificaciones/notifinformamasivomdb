package pe.com.claro.eai.notificainfomasivo.util;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

import javax.xml.ws.Service;

import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

public class CustomJaxWsPortProxyFactoryBean extends JaxWsPortProxyFactoryBean {
	@Override
	public Service createJaxWsService() {
		Authenticator.setDefault(new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(getUsername(), getPassword().toCharArray());
			}
		});
		return super.createJaxWsService();
	}
}