package pe.com.claro.eai.notificainfomasivo.bean;

import java.util.List;
 

public class ObtenerConfigPlantilla {

	private String errorCode;
	private String errorMsg;
	private ListaMayor listaPlantillas;

	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public ListaMayor getListaPlantillas() {
		return listaPlantillas;
	}
	public void setListaPlantillas(ListaMayor listaPlantillas) {
		this.listaPlantillas = listaPlantillas;
	}
	
}
