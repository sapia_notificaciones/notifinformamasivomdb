package pe.com.claro.eai.notificainfomasivo.bean;

public class EstructuraCampos {

	private String tipoDocumento;
	private String documento;
	private String parametro1;
	private String parametro2;
	private String parametro3;
	private String parametro4;
	private String parametro5;
	private String parametro6;
	private String parametro7;
	private String parametro8;
	private String parametro9;
	private String parametro10;
	private String parametro11;
	private String parametro12;
	private String parametro13;
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getParametro1() {
		return parametro1;
	}
	public void setParametro1(String parametro1) {
		this.parametro1 = parametro1;
	}
	public String getParametro2() {
		return parametro2;
	}
	public void setParametro2(String parametro2) {
		this.parametro2 = parametro2;
	}
	public String getParametro3() {
		return parametro3;
	}
	public void setParametro3(String parametro3) {
		this.parametro3 = parametro3;
	}
	public String getParametro4() {
		return parametro4;
	}
	public void setParametro4(String parametro4) {
		this.parametro4 = parametro4;
	}
	public String getParametro5() {
		return parametro5;
	}
	public void setParametro5(String parametro5) {
		this.parametro5 = parametro5;
	}
	public String getParametro6() {
		return parametro6;
	}
	public void setParametro6(String parametro6) {
		this.parametro6 = parametro6;
	}
	public String getParametro7() {
		return parametro7;
	}
	public void setParametro7(String parametro7) {
		this.parametro7 = parametro7;
	}
	public String getParametro8() {
		return parametro8;
	}
	public void setParametro8(String parametro8) {
		this.parametro8 = parametro8;
	}
	public String getParametro9() {
		return parametro9;
	}
	public void setParametro9(String parametro9) {
		this.parametro9 = parametro9;
	}
	public String getParametro10() {
		return parametro10;
	}
	public void setParametro10(String parametro10) {
		this.parametro10 = parametro10;
	}
	public String getParametro11() {
		return parametro11;
	}
	public void setParametro11(String parametro11) {
		this.parametro11 = parametro11;
	}
	public String getParametro12() {
		return parametro12;
	}
	public void setParametro12(String parametro12) {
		this.parametro12 = parametro12;
	}
	public String getParametro13() {
		return parametro13;
	}
	public void setParametro13(String parametro13) {
		this.parametro13 = parametro13;
	}
	
	
	
}
