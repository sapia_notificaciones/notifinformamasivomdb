package pe.com.claro.eai.notificainfomasivo.proxy;

import javax.xml.datatype.XMLGregorianCalendar;

import com.comviva.ngage.soap.sms.SmsReq.JobType;
import com.comviva.ngage.soap.sms.SmsReq.Sender;

import pe.com.claro.eai.notificainfomasivo.exception.WSException;

import com.comviva.ngage.soap.sms.SMSSubmitResponse;



public interface EnviaSMSCliente {

	SMSSubmitResponse enviarSMS(String mensajeTransaccion, String nombreCampanha, String descripcionCampanha, String idProtocolo, String categoriaCampanha, 
			String categoriaPromocional, Double tipoContenido, XMLGregorianCalendar fechaProgramadaDeEnvio, XMLGregorianCalendar fechaPeriodoValidez,
			Boolean reporteDeEnvio, Sender remitente, JobType tipoTrabajo, String callBackUrl, String transaccionID) throws WSException;

}
