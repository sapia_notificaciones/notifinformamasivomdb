package pe.com.claro.eai.notificainfomasivo.bean;

public class ClientesAPP {

	private String idNotificacion;
	private String identificador;
	
	public String getIdNotificacion() {
		return idNotificacion;
	}
	public void setIdNotificacion(String idNotificacion) {
		this.idNotificacion = idNotificacion;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	
	
}
