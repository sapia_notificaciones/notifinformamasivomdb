package pe.com.claro.eai.notificainfomasivo.dao;

import java.util.List;

import pe.com.claro.eai.notificainfomasivo.bean.ActualizarEstLoteRequest;
import pe.com.claro.eai.notificainfomasivo.bean.BeanRegistrarBaseHistorico;
import pe.com.claro.eai.notificainfomasivo.bean.ObtenerConfigPlantilla;
import pe.com.claro.eai.notificainfomasivo.bean.ResponseClientesApp;
import pe.com.claro.eai.notificainfomasivo.bean.Retorno;
import pe.com.claro.eai.notificainfomasivo.exception.DBException;
import pe.com.claro.eai.notificainfomasivo.exception.DBNoDisponibleException;
import pe.com.claro.eai.notificainfomasivo.exception.DBTimeoutException;

public interface NotifDBDAO {

	
	public Retorno registrarIniProceso(String mensajeLogMet, String idTransaccion,  String destinatario, String componente, String notificacionXML)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException;
	
	public Retorno registrarError(String mensajeLogMet, String idTransaccion, String componente, String codError, String msjError);
	
	public ObtenerConfigPlantilla obtenerConfiguracionPlantilla(String mensajeLogMet, String idTransaccion , String fechaEjecucion)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException;
	
	public Retorno actualizarFinProceso(String mensajeLogMet,String idNotiDestinatario)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException;
	
	public ResponseClientesApp buscarClientePorApp (String mensajeLogMet,String idNotificacion)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException;
	
	public Retorno insertCargaHistorico(String mensajeLogMet,String idTransaccion, List<BeanRegistrarBaseHistorico> lista)
			throws Exception;
	
	public Retorno actualizarEstadoLote(String mensajeLogMet,String idTransaccion, ActualizarEstLoteRequest request)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException;;
	
	
	
	
}
