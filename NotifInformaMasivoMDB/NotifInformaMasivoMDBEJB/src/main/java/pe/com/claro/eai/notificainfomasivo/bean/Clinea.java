package pe.com.claro.eai.notificainfomasivo.bean;

public class Clinea {

	String idNotificacion;
	String identificador;
	
	@Override
	public String toString() {
		return "Clinea [idNotificacion=" + idNotificacion + ", identificador=" + identificador + "]";
	}
	public String getIdNotificacion() {
		return idNotificacion;
	}
	public void setIdNotificacion(String idNotificacion) {
		this.idNotificacion = idNotificacion;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	
}
