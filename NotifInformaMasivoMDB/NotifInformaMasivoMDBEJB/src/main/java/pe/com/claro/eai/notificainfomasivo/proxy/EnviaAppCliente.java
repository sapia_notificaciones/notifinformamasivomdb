package pe.com.claro.eai.notificainfomasivo.proxy;

import pe.com.claro.eai.notificainfomasivo.bean.createCampaingApp.request.CreateCampaignAppRequest;
import pe.com.claro.eai.notificainfomasivo.bean.createCampaingApp.request.response.CreateCampaignAppResponse;
import pe.com.claro.eai.notificainfomasivo.exception.WSException;

public interface EnviaAppCliente {

	CreateCampaignAppResponse enviarApp(String idTransaccion,String token,String mensajeTransaccion,CreateCampaignAppRequest request)throws WSException;
	
}
