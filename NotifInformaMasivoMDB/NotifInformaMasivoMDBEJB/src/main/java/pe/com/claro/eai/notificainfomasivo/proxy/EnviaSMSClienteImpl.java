package pe.com.claro.eai.notificainfomasivo.proxy;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.comviva.ngage.soap.sms.SmsReq;
import com.comviva.ngage.soap.sms.SmsReq.JobType;
import com.comviva.ngage.soap.sms.SmsReq.Sender;

import pe.com.claro.eai.notificainfomasivo.dao.NotifDBDAO;
import pe.com.claro.eai.notificainfomasivo.exception.WSException;
import pe.com.claro.eai.notificainfomasivo.util.Constantes;
import pe.com.claro.eai.notificainfomasivo.util.PropertiesExterno;
import pe.com.claro.eai.notificainfomasivo.util.UtilJava;

import com.comviva.ngage.soap.sms.SMSSubmitRequest;
import com.comviva.ngage.soap.sms.SMSSubmitResponse;
import com.comviva.ngage.soap.sms.SMSWebService;

@Service
public class EnviaSMSClienteImpl implements EnviaSMSCliente {

	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	@Qualifier("sMSWebService")
	private SMSWebService smsWebService;

	@Autowired
	private NotifDBDAO notifDB;
	
	@Autowired
	private PropertiesExterno constantesexternos;

	@Override
	public SMSSubmitResponse enviarSMS(String mensajeTransaccion, String nombreCampanha, String descripcionCampanha, String idProtocolo, String categoriaCampanha, 
			String categoriaPromocional, Double tipoContenido, XMLGregorianCalendar fechaProgramadaDeEnvio, XMLGregorianCalendar fechaPeriodoValidez,
			Boolean reporteDeEnvio, Sender remitente, JobType tipoTrabajo, String callBackUrl, String transaccionID) throws WSException {
		String mensajeMetodo = mensajeTransaccion + "[enviarSMS] ";
		SMSSubmitResponse response = null;
		SMSSubmitRequest request = new SMSSubmitRequest();
		try {
			SmsReq req = new SmsReq();
			req.setCampaignName(nombreCampanha);
			req.setCampaignDesc(descripcionCampanha);
			req.setProtocolId(idProtocolo);
			req.setCampaignCategory(categoriaCampanha);
			req.setPromotionalCategory(categoriaPromocional);
			req.setContentType(tipoContenido);
			req.setScheduledDeliveryDateTime(fechaProgramadaDeEnvio);
			req.setValidityPeriodDateTime(fechaPeriodoValidez);
			req.setDeliveryReport(reporteDeEnvio);
			req.setSender(remitente);
			req.setJobType(tipoTrabajo);
			//Cambios 04-01-2019
			req.setCallBackURL(callBackUrl);
			request.setReq(req);
			Holder<String> headers = new Holder<>();
			headers.value = transaccionID;
			logger.info(mensajeMetodo + "Se invocara al Servicio: " + constantesexternos.WS_ENVIASMS_ENDPOINTADDRESS);
			logger.info(mensajeMetodo + "Datos de entrada al Servicio Header: " + UtilJava.anyObjectToXmlText(headers));
			logger.info(mensajeMetodo + "Datos de entrada al Servicio: " + UtilJava.anyObjectToXmlText(request));
			response = smsWebService.smsSubmit(request, headers);
			logger.info(mensajeMetodo + "Datos de salida del Servicio: " + UtilJava.anyObjectToXmlText(response));
		} catch (Exception e) {
			logger.error(mensajeMetodo + "ERROR: [Exception] - [" + e.getMessage() + "] ");
			String excepcion = e + "";
			String msjError = null;
			String codError = null;
			if (excepcion.contains(constantesexternos.WS_ENVIASMS_ERROR_TIME_OUT)) {
				codError = constantesexternos.WS_ENVIASMS_CODIGO_IDT1;
				msjError = String.format(constantesexternos.WS_ENVIASMS_MENSAJE_IDT1, constantesexternos.WS_ENVIASMS_ENDPOINTADDRESS) + " " + e.getMessage();
			} else {
				codError = constantesexternos.WS_ENVIASMS_CODIGO_IDT2;
				msjError = String.format(constantesexternos.WS_ENVIASMS_MENSAJE_IDT2, constantesexternos.WS_ENVIASMS_ENDPOINTADDRESS) + " " + e.getMessage();
			}
			
			notifDB.registrarError(mensajeTransaccion, transaccionID, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_GESTOR_SMS, msjError);
			
			throw new WSException(codError, msjError, e);
		}
		return response;
	}
}
