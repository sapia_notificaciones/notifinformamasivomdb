package pe.com.claro.eai.notificainfomasivo.exception;

public class WSException extends BaseException {
	private static final long serialVersionUID = 1L;
	private String nombreWS;

	public WSException(String code, String message, Exception exception) {
        super(code, message, exception);
    }
	
	public WSException(String codError, String nombreWS, String msjError, Exception objException) {
		super(codError, msjError, objException);
		this.nombreWS = nombreWS;
	}

	public WSException(String codError, String nombreWS, String msjError, Throwable objException) {
		super(codError, msjError, objException);
		this.nombreWS = nombreWS;
	}

	public WSException(String msjError, Exception objException) {
		super(msjError, objException);
	}

	public WSException(Exception objException) {
		super(objException);
	}

	public WSException(String msjError) {
		super(msjError);
	}

	public String getNombreWS() {
		return nombreWS;
	}

	public void setNombreWS(String nombreWS) {
		this.nombreWS = nombreWS;
	}

}
