package pe.com.claro.eai.notificainfomasivo.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.comviva.ngage.soap.sms.SMSSubmitResponse;
import com.comviva.ngage.soap.sms.SmsReq.JobType;
import com.comviva.ngage.soap.sms.SmsReq.Sender;

import pe.com.claro.eai.notificainfomasivo.bean.ActualizarEstLoteRequest;
import pe.com.claro.eai.notificainfomasivo.bean.BeanRegistrarBaseHistorico;
import pe.com.claro.eai.notificainfomasivo.bean.ObtenerConfigPlantilla;
import pe.com.claro.eai.notificainfomasivo.bean.PlantillaSMS;
import pe.com.claro.eai.notificainfomasivo.bean.Retorno;
import pe.com.claro.eai.notificainfomasivo.dao.NotifDBDAO;
import pe.com.claro.eai.notificainfomasivo.exception.SFTPException;
import pe.com.claro.eai.notificainfomasivo.proxy.EnviaSMSCliente;
import pe.com.claro.eai.notificainfomasivo.util.Constantes;
import pe.com.claro.eai.notificainfomasivo.util.PropertiesExterno;
import pe.com.claro.eai.notificainfomasivo.util.UtilFTP;
import pe.com.claro.eai.notificainfomasivo.util.Utilitarios;

import org.slf4j.LoggerFactory;

@Service
public class NotiInfoMasivoMDBServiceImpl implements NotiInfoMasivoMDBService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	private PropertiesExterno propertiesExterno;

	@Autowired
	private UtilFTP utilFTP;

	@Autowired
	private NotifDBDAO notifDB;

	@Autowired
	private Utilitarios util;

	@Autowired
	private EnviaSMSCliente enviaSMSClient;
	
	String codError="";
	String msjeError="";
	
	@Override
	public void procesar(String mensajeTransversal, String idTransaccion, String nombreFichero) {

		long tiempoInicio = System.currentTimeMillis();

		Retorno retornoRegistrarIniProceso = null;

		try {

			String nombreArchivo = Constantes.VACIO;
			String[] separador = nombreFichero.split(Constantes.CARACTER_SLASH);
			for (int i = 0; i < separador.length; i++) {

				if (separador[i].contains(Constantes.FORMATO_TXT)) {

					nombreArchivo = separador[i];
					
				}

			}
			
			logger.info(mensajeTransversal + " [INICIO]-[Actividad 1. Registrar inicio de procesamiento]");

			retornoRegistrarIniProceso = notifDB.registrarIniProceso(mensajeTransversal, idTransaccion, nombreArchivo,
					propertiesExterno.cCOMPONENTE_REGISTRO_INI_PROCESO, Constantes.CADENA_VACIA);

			logger.info(mensajeTransversal + " [FIN]-[Actividad 1. Registrar inicio de procesamiento]");

			if (retornoRegistrarIniProceso.getErrorCode()
					.equals(propertiesExterno.cNOTIFDB_RPTA_REGISTRAR_INI_PROCESO)) {

				logger.info(mensajeTransversal + " [INICIO]-[Actividad 2.  Conectarse a Repositorio]");

				validarNroFilas(mensajeTransversal, idTransaccion, nombreArchivo);

			}else {
				
				codError = retornoRegistrarIniProceso.getErrorCode();
				msjeError = retornoRegistrarIniProceso.getErrorMsg();
				
				notifDB.registrarError(mensajeTransversal, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, codError, msjeError);
				
				
			}

		} catch (Exception e) {

			logger.error(mensajeTransversal + "[Error Generado:]" + e);

			codError = Constantes.CODIGO_ERROR_EXCEPTION_GENERAL;
			msjeError = e.toString();
			
			notifDB.registrarError(mensajeTransversal, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, codError, msjeError);

			
		} finally {
			utilFTP.desconectarSFTP(mensajeTransversal);
			logger.info(mensajeTransversal + "[FIN][TiempoTotalTransaccion(ms):"
					+ (System.currentTimeMillis() - tiempoInicio) + "]");
			logger.info(mensajeTransversal + "[FIN] - [METODO: procesar] ");
		}

	}

	public boolean validarNroFilas(String mensajeTransversal, String idTransaccion, String nombreArchivo)
			throws Exception {

		boolean permisoOK = false;
		List<String> listaCarpetas = null;
		long nroFilas = 0;
		boolean conexion = false;
		boolean obtenerNroFilas = false;

		conexion = utilFTP.conectarSFTP(mensajeTransversal, propertiesExterno.cCARGA_SFTP_IP,
				propertiesExterno.cCARGA_SFTP_USUARIO, propertiesExterno.cCARGA_SFTP_CONTRASENA,
				propertiesExterno.cCARGA_SFTP_PUERTO, propertiesExterno.cCARGA_SFTP_TIMEOUT);

		logger.info(mensajeTransversal + " [FIN]-[Actividad 2.  Conectarse a Repositorio]");

		if (conexion) {

			logger.info(mensajeTransversal + " Se obtuvo conexion a SFTP");

			logger.info(mensajeTransversal + " [Inicio]-[Actividad 3. Validar permiso de lectura/escritura]");

			String[] arrCarpetas = propertiesExterno.cCARGA_SFTP_LISTA_CARPETAS.split(Constantes.VALOR_PALOTE_UNIC);

			listaCarpetas = Arrays.asList(arrCarpetas);

			permisoOK = utilFTP.validarPermisosCarpetas(mensajeTransversal,idTransaccion, propertiesExterno.cCARGA_SFTP_RUTA_BASE,
					listaCarpetas, propertiesExterno.cCARGA_SFTP_PERMISO_LISTA_CARPETAS,
					propertiesExterno.cCARGA_SFTP_USUARIO);

			logger.info(mensajeTransversal + " [Fin]-[Actividad 3. Validar permiso de lectura/escritura]");

			nroFilas = util.obtenerRegistros(mensajeTransversal, idTransaccion, permisoOK, nombreArchivo);

			obtenerNroFilas = validarProceso(mensajeTransversal, idTransaccion, nroFilas, nombreArchivo);

		}else {
			
			codError = Constantes.CODIGO_ERROR_CONECTIVIDAD;
			msjeError = Constantes.MSJE_ERROR_CONECTIVIDAD;
			
			notifDB.registrarError(mensajeTransversal, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, codError, msjeError);

		}

		logger.info(mensajeTransversal + " [FIN]-[Actividad15. Se termina el proceso]");

		return obtenerNroFilas;

	}

	public boolean validarProceso(String mensajeTransversal, String idTransaccion, long nroFilas, String nombreFichero)
			throws Exception {

		ObtenerConfigPlantilla obtenerConfigPlantilla = null;
		List<BeanRegistrarBaseHistorico> listRegistroHistSMS = null;
		String plantilla = "";
		List<String> filesNames = null;
		boolean terminoProceso = false;
		String codigoMensaje = "";
		String msjeSMS = "";
		SimpleDateFormat formatoDia = new SimpleDateFormat(propertiesExterno.FECHADIA_FORMATO);
		String fechaDia = formatoDia.format(new Date());
		boolean validarProceso = false;
		boolean insertSMSOK = false;
		ArrayList<String> listCampos=new ArrayList<>();
		ArrayList<PlantillaSMS> listaPlantillaSMS = null;
		String msjeSMSEnvio = "";
		String scheduledDelyDate = "";
		String validPeriodDate = "";

		if (nroFilas > 0) {

			filesNames = utilFTP.particionarFicheroFTP(mensajeTransversal,idTransaccion,
					propertiesExterno.cCARGA_SFTP_SIN_PROCESAR,
					nombreFichero,propertiesExterno.cCARGA_SFTP_POR_PROCESAR,
					nombreFichero.substring(0, nombreFichero.length() - 4), nroFilas);

			logger.info(mensajeTransversal + "Se particionaron a " + filesNames.size() + " archivos");

			obtenerConfigPlantilla = notifDB.obtenerConfiguracionPlantilla(mensajeTransversal, idTransaccion, fechaDia);

			plantilla = obtenerConfigPlantilla.getListaPlantillas().getRow().getCodigoPlantilla();
			
			listaPlantillaSMS = obtenerConfigPlantilla.getListaPlantillas().getRow().getPlantillasSms();
			
			for (int i = 0; i < filesNames.size(); i++) {

				for (int j = 0; j < listaPlantillaSMS.size(); j++) {
					
					String nombreFileSMS = filesNames.get(i).substring(0, filesNames.get(i).length() - 4)+Constantes.GUION_BAJO+(j+1)+Constantes.FORMATO_TXT;
					
					codigoMensaje = obtenerConfigPlantilla.getListaPlantillas().getRow().getPlantillasSms().get(j).getCodigo();
					msjeSMS = obtenerConfigPlantilla.getListaPlantillas().getRow().getPlantillasSms().get(j).getMensaje();
					msjeSMSEnvio = propertiesExterno.CODIGO_INTERNACIONAL_PERU + Constantes.LLAVE_IZQUIERDA + Constantes.CAMPO_NROTELEFONICO + 
									Constantes.LLAVE_DERECHA+Constantes.COMILLA_DOBLE + Constantes.COMA + propertiesExterno.cTIPO_ENVIO_SMS + Constantes.COMA + Constantes.SEPARADOR_BACKSLASH_UNICODE + Constantes.COMILLA_DOBLE + msjeSMS +Constantes.SEPARADOR_BACKSLASH_UNICODE +Constantes.COMILLA_DOBLE+ Constantes.COMILLA_DOBLE;
					scheduledDelyDate = obtenerConfigPlantilla.getListaPlantillas().getRow().getPlantillasSms().get(j).getDeliver();
					validPeriodDate = obtenerConfigPlantilla.getListaPlantillas().getRow().getPlantillasSms().get(j).getPeriod();
					
					listCampos = utilFTP.leerArchivoFTP(mensajeTransversal,idTransaccion, propertiesExterno.cCARGA_SFTP_POR_PROCESAR,
							filesNames.get(i));
					
					listRegistroHistSMS = llenarCampos(mensajeTransversal,idTransaccion, listCampos, codigoMensaje, fechaDia, i);

					insertSMSOK = insertHistorico(mensajeTransversal,idTransaccion, listRegistroHistSMS);

					if (insertSMSOK) {

						recorrerListCampos(mensajeTransversal,idTransaccion, msjeSMSEnvio, plantilla, filesNames.get(i),j+1);
						
						boolean envioGestorSMS = enviarGestorSMS(mensajeTransversal, idTransaccion,
								propertiesExterno.WSENVIOSMSCARPETA + Constantes.CARACTER_SLASH
										+ nombreFileSMS,scheduledDelyDate,validPeriodDate);

						if (envioGestorSMS) {

							actualizarLote(mensajeTransversal, idTransaccion, i, Constantes.CODIGO_EXITO_HIST);

						} else {

							actualizarLote(mensajeTransversal, idTransaccion, i, Constantes.CODIGO_ERROR_HIST);

						}

						terminoProceso = true;
					}
					
				}


			}

			if (terminoProceso) {

				notifDB.actualizarFinProceso(mensajeTransversal, idTransaccion);
				utilFTP.eliminarFicheroSFTP(mensajeTransversal,idTransaccion, propertiesExterno.cCARGA_SFTP_SIN_PROCESAR);
				utilFTP.eliminarFicheroSFTP(mensajeTransversal,idTransaccion,  propertiesExterno.cCARGA_SFTP_POR_PROCESAR);
				validarProceso = true;

			}

		} else {

			logger.info(mensajeTransversal + " No existen registros a procesar");
			codError = Constantes.CODIGO_ERROR_SINREGISTROS;
			msjeError = Constantes.MSJE_ERROR_SINREGISTROS;
			
			notifDB.registrarError(mensajeTransversal, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, codError, msjeError);

		}

		return validarProceso;

	}

	public List<BeanRegistrarBaseHistorico> llenarCampos(String mensajeTransversal,String idTransaccion, ArrayList<String> listCampos,
			String codigoMensaje, String fechaDia, int i) {

		List<BeanRegistrarBaseHistorico> listRegistroHistSMS = null;

		if (!listCampos.isEmpty()) {

			listRegistroHistSMS = listarBeanRegisHistorico(listCampos, idTransaccion, codigoMensaje,
					propertiesExterno.CANAL_SMSNG, fechaDia, i + 1);

			logger.info(mensajeTransversal + "Se encontraron " + listCampos.size() + " registros en el archivo, se procesa a llenar la lista");

		} else {

			logger.info(mensajeTransversal +"No se encontraron registros en el archivo");

		}

		return listRegistroHistSMS;

	}

	public List<BeanRegistrarBaseHistorico> listarBeanRegisHistorico(ArrayList<String> listCampos,
			String idNotificacion, String tipo, String canal, String fecha, long loteArchivo) {

		List<BeanRegistrarBaseHistorico> listBeanRegistrarBaseHistorico = new ArrayList<>();

		for (int i = 0; i < listCampos.size(); i++) {

			String[] campos = listCampos.get(i).split(Constantes.VALOR_PALOTE_UNIC); 
									
			BeanRegistrarBaseHistorico beanRegistroHistorico = new BeanRegistrarBaseHistorico();
			beanRegistroHistorico.setIdNotificacion(idNotificacion);
			beanRegistroHistorico.setIdentificador(campos[2]);
			beanRegistroHistorico.setCanal(canal);
			beanRegistroHistorico.setTipo(tipo);
			beanRegistroHistorico.setEstado(0);
			beanRegistroHistorico.setFecha(fecha);
			beanRegistroHistorico.setLote((int) loteArchivo);

			listBeanRegistrarBaseHistorico.add(beanRegistroHistorico);

		}

		return listBeanRegistrarBaseHistorico;

	}

	public boolean insertHistorico(String mensajeTransversal,String idTransaccion, List<BeanRegistrarBaseHistorico> listRegistroHistSMS)
			throws Exception {

		boolean insertSMSOK = false;
		Retorno registroHistoricoSMS = null;

		if (listRegistroHistSMS != null && !listRegistroHistSMS.isEmpty()) {

			registroHistoricoSMS = notifDB.insertCargaHistorico(mensajeTransversal,idTransaccion, listRegistroHistSMS);

		} else {

			logger.info(mensajeTransversal +"No se encontraron registros en el Historico");

		}

		if (registroHistoricoSMS != null && registroHistoricoSMS.getErrorCode().equals(Constantes.CODIGO_EXITO)) {

			insertSMSOK = true;

		}

		return insertSMSOK;

	}

	public void recorrerListCampos(String mensajeTransversal,String idTransaccion, String msjeSMS, String plantilla,String nombreFichero,int nroFicheroMsje) throws SFTPException, IOException {


		String msje = util.cruzarBaseConPlantilla(mensajeTransversal,msjeSMS, plantilla);
		
		
		 utilFTP.generarFicheroPorColumnas(mensajeTransversal,idTransaccion,propertiesExterno.cCARGA_SFTP_POR_PROCESAR,
				nombreFichero,propertiesExterno.cCARGA_SFTP_CARPETA_FICHERO_PROCESADOS+Constantes.CARACTER_SLASH+nombreFichero, msje,nroFicheroMsje);
			
	}

	public boolean enviarGestorSMS(String msjTx, String idTransaccion, String fileName,String scheduledDelyDate,String validPeriodDate) {

		SMSSubmitResponse smsResponse = null;

		boolean envioGestorSMS = false;

		logger.info(msjTx + "[Actividad 10.  Enviar a programar al Gestor de Notificaciones SMS - Inicio ]");

		try {
			Sender sender = new Sender();
			sender.setUsername(propertiesExterno.WSSMSREMITENTEUSER);
			sender.setPassword(propertiesExterno.WSREMITENTEPASSWORD);
			sender.setAddress(propertiesExterno.WSSMSREMITENTEADDRESS);
			JobType.File file = new JobType.File();
			file.setDataFileURL(propertiesExterno.WSSMSTYPESHIPPING + Constantes.SIGNO_DOS_PUNTOS
					+ Constantes.CARACTER_DOBLESLASH + propertiesExterno.cCARGA_SFTP_USUARIO
					+ Constantes.SIGNO_DOS_PUNTOS + propertiesExterno.cCARGA_SFTP_CONTRASENA
					+ Constantes.CARACTER_ARROBA + propertiesExterno.cCARGA_SFTP_IP + ":"
					+ propertiesExterno.cCARGA_SFTP_PUERTO + fileName);
			JobType job = new JobType();
			job.setFile(file);
			XMLGregorianCalendar fechaProgramadaDeEnvio = util
					.obtenerFechaProgramadaGMT(scheduledDelyDate);
			XMLGregorianCalendar fechaPeriodoValidez = util
					.obtenerFechaProgramadaGMT(validPeriodDate);
						
			logger.info(msjTx + ReflectionToStringBuilder.toString(file));

			smsResponse = enviaSMSClient.enviarSMS(msjTx, propertiesExterno.WSSMSCAMPAIGNNAME,
					propertiesExterno.WSSMSCAMPAIGNDESC, propertiesExterno.WSSMSPROTOCOLID,
					propertiesExterno.WSSMSCAMPAIGNCATEG, propertiesExterno.WSSMSPROMOCATEG,
					Double.parseDouble(propertiesExterno.WSSMSCONTENTTYPE), fechaProgramadaDeEnvio, fechaPeriodoValidez,
					new Boolean(propertiesExterno.WSSMSDELIVERYREPORT), sender, job, propertiesExterno.WSSMSCALLBACKURL,
					idTransaccion);

			if (smsResponse != null && smsResponse.getRsp().getStatus().getStatusCode() == 201) {

				envioGestorSMS = true;

			}

		} catch (Exception e) {

			logger.info(msjTx + e.toString());

			
		}

		logger.info(msjTx + "[Actividad 10.  Enviar a programar al Gestor de Notificaciones SMS - Fin ]");

		return envioGestorSMS;

	}

	public boolean actualizarLote(String mensajeTransversal, String idTransaccion, int i, String estado) {

		boolean actualizarLote = false;

		try {

			ActualizarEstLoteRequest actEstRequest = new ActualizarEstLoteRequest();
			actEstRequest.setIdNotificacion(idTransaccion);
			actEstRequest.setCanal(propertiesExterno.CANAL_SMSNG);
			actEstRequest.setEstado(estado);
			actEstRequest.setLote(i + 1);

			notifDB.actualizarEstadoLote(mensajeTransversal,idTransaccion,actEstRequest);
			actualizarLote = true;

		} catch (Exception e) {
			logger.error(e.toString());
		}

		return actualizarLote;

	}

}
