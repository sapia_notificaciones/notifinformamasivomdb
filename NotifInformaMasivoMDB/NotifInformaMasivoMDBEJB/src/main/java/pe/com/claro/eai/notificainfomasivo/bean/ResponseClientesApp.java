package pe.com.claro.eai.notificainfomasivo.bean;

import java.util.List;

public class ResponseClientesApp {

	private String errorCode;
	private String errorMsg;
	private List<Clinea> listClientesApp;
	
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public List<Clinea> getListClientesApp() {
		return listClientesApp;
	}
	public void setListClientesApp(List<Clinea> listClientesApp) {
		this.listClientesApp = listClientesApp;
	}

	
	
	
	
}
