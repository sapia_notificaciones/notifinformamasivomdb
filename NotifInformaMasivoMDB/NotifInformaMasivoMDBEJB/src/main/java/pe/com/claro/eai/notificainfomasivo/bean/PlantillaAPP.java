package pe.com.claro.eai.notificainfomasivo.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "APP_LISTA_ROW")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlantillaAPP {
	
	@XmlElement(name="TCFGV_CODCANAL")	
	private String codigo;
	@XmlElement(name="TCFGV_CODCATEGORIA")
	private String codigoCategoria;
	@XmlElement(name="TCFGV_NOMBRECAMP")
	private String nombreCampania;
	@XmlElement(name="TCFGV_TIPO")
	private String tipo;
	@XmlElement(name="TCFGV_PLAN_TITULO")
	private String titulo;
	@XmlElement(name="TCFGV_PLAN_DESC_PUSH")
	private String descPush;
	@XmlElement(name="TCFGV_PLAN_DESC_LARGA")
	private String descLarga;
	@XmlElement(name="TCFGV_PLAN_DESC_CORTA")
	private String descCorta;
	@XmlElement(name="TCFGV_COD_USUARIO")
	private String codigoUsuario;
	@XmlElement(name="TCFGN_VIGENCIA")
	private int vigencia;
	@XmlElement(name="TCFGV_APL_ENTREGA")
	private String aplicacionesEntrega;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodigoCategoria() {
		return codigoCategoria;
	}
	public void setCodigoCategoria(String codigoCategoria) {
		this.codigoCategoria = codigoCategoria;
	}
	public String getNombreCampania() {
		return nombreCampania;
	}
	public void setNombreCampania(String nombreCampania) {
		this.nombreCampania = nombreCampania;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescPush() {
		return descPush;
	}
	public void setDescPush(String descPush) {
		this.descPush = descPush;
	}
	public String getDescLarga() {
		return descLarga;
	}
	public void setDescLarga(String descLarga) {
		this.descLarga = descLarga;
	}
	public String getDescCorta() {
		return descCorta;
	}
	public void setDescCorta(String descCorta) {
		this.descCorta = descCorta;
	}
	public String getCodigoUsuario() {
		return codigoUsuario;
	}
	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	public int getVigencia() {
		return vigencia;
	}
	public void setVigencia(int vigencia) {
		this.vigencia = vigencia;
	}
	public String getAplicacionesEntrega() {
		return aplicacionesEntrega;
	}
	public void setAplicacionesEntrega(String aplicacionesEntrega) {
		this.aplicacionesEntrega = aplicacionesEntrega;
	}
	
}
