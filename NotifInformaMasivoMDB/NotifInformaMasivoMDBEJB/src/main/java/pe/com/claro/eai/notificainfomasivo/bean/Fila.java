package pe.com.claro.eai.notificainfomasivo.bean;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ROW")
@XmlAccessorType(XmlAccessType.FIELD)
public class Fila {

	@XmlElement(name = "TCFGV_CODTPL")
	private String codigoPlantilla;

	@XmlElementWrapper(name = "SMS_LISTA")
	@XmlElement(name = "SMS_LISTA_ROW")
	private ArrayList<PlantillaSMS> plantillasSms;

	@XmlElementWrapper(name = "APP_LISTA")
	@XmlElement(name = "APP_LISTA_ROW")
	private ArrayList<PlantillaAPP> plantillasApp;

	public String getCodigoPlantilla() {
		return codigoPlantilla;
	}

	public void setCodigoPlantilla(String codigoPlantilla) {
		this.codigoPlantilla = codigoPlantilla;
	}

	public ArrayList<PlantillaSMS> getPlantillasSms() {
		return plantillasSms;
	}

	public void setPlantillasSms(ArrayList<PlantillaSMS> plantillasSms) {
		this.plantillasSms = plantillasSms;
	}

	public ArrayList<PlantillaAPP> getPlantillasApp() {
		return plantillasApp;
	}

	public void setPlantillasApp(ArrayList<PlantillaAPP> plantillasApp) {
		this.plantillasApp = plantillasApp;
	}

}
