package pe.com.claro.eai.notificainfomasivo.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Utilitarios {

	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Autowired
	private UtilFTP utilFTP;
	
	public XMLGregorianCalendar obtenerFechaProgramadaGMT(String fecha){
		
		String[] cFechaProgramada = fecha.split(":");
		
		Calendar calFechaProgramadaDeEnvio = Calendar.getInstance();
		calFechaProgramadaDeEnvio.set(Calendar.HOUR_OF_DAY,Integer.parseInt(cFechaProgramada[0]));
		calFechaProgramadaDeEnvio.set(Calendar.MINUTE,Integer.parseInt(cFechaProgramada[1]));
		calFechaProgramadaDeEnvio.set(Calendar.SECOND,Integer.parseInt(cFechaProgramada[2]));
		
		Date dFechaProgramada = calFechaProgramadaDeEnvio.getTime();

		SimpleDateFormat formatoFechaProgram=  new SimpleDateFormat(propertiesExterno.NGAGEFORMATOFECHAHORA, Locale.US);
		formatoFechaProgram.setTimeZone(TimeZone.getTimeZone(Constantes.TIMEZONEGMT));
		
		XMLGregorianCalendar fechaProgramadaDeEnvio = null;
		
		if (!cFechaProgramada.equals(Constantes.VACIO)) {

			try {
				fechaProgramadaDeEnvio = DatatypeFactory.newInstance()
						.newXMLGregorianCalendar(formatoFechaProgram.format(dFechaProgramada));
			} catch (DatatypeConfigurationException e) {
				
				e.printStackTrace();
				logger.info(e.toString());
			}

		}
		
		return fechaProgramadaDeEnvio;
		
	}
	
	public long obtenerRegistros(String mensajeTransversal, String idTransaccion, boolean permisoOK,
			String nombreFichero) {
		long numRegFicheroFTPBase = 0;
		long tamanioFicheroFTPBase = 0;
		long numRegFicheroFTPPorProcesar = 0;

		logger.info(mensajeTransversal + "[Inicio] - [METODO: ObtenerRegistros] ");

		try {

			if (permisoOK) {
				logger.info(mensajeTransversal + "Los directorios cuenta con permisos en el SFTP");
				
				numRegFicheroFTPBase = utilFTP.obtenerNumeroRegistrosFTP(mensajeTransversal,idTransaccion,
						propertiesExterno.cCARGA_SFTP_SIN_PROCESAR, nombreFichero);
				
				logger.info(mensajeTransversal + "numRegFicheroFTPBase:" + numRegFicheroFTPBase);

				tamanioFicheroFTPBase = utilFTP.obtenerTamanioFicheroFTP(mensajeTransversal,idTransaccion,
						propertiesExterno.cCARGA_SFTP_SIN_PROCESAR, nombreFichero);
				
				logger.info(mensajeTransversal + "tamanioFicheroFTPBase:" + tamanioFicheroFTPBase);

				numRegFicheroFTPPorProcesar = determinarRegistroPorFichero(mensajeTransversal,
						Integer.parseInt(propertiesExterno.cCARGA_SFTP_MAX_TAM_FICHERO_POR_PROC), tamanioFicheroFTPBase,
						numRegFicheroFTPBase);
				
				
				
				logger.info(mensajeTransversal + "Cada fichero tendra " + numRegFicheroFTPPorProcesar + " registros por fichero");

				logger.info(mensajeTransversal + " [Fin]-[Actividad 5. Particionar Archivo Base]");

			} else {

				logger.info(mensajeTransversal + " No se obtuvo permisos para las carpetas en SFTP");

			}

		} catch (Exception e) {
			logger.error(mensajeTransversal + "[Error Generado:]" + e);
		} finally {
			logger.info(mensajeTransversal + "[FIN] - [METODO: ObtenerRegistros] ");

		}
		return numRegFicheroFTPPorProcesar;

	}

	public long determinarRegistroPorFichero(String mensajeTransversal, long pesoMaximoFichero, long pesoficheroActual,
			long totalRegistrosFichero) {
		double cantidadFicherosTemp = 0;
		double residuo = 0;
		long registrosPorFichero = 0;
		int numeroFicheros;
		
		logger.info(mensajeTransversal + "[Inicio] - [METODO: determinarRegistroPorFichero] ");

		try {
			cantidadFicherosTemp = (pesoficheroActual / pesoMaximoFichero);
			residuo = pesoficheroActual % pesoMaximoFichero;

			if (cantidadFicherosTemp < 1) {
				registrosPorFichero = totalRegistrosFichero;

			} else if (cantidadFicherosTemp >= 1 && residuo != 0) {

				numeroFicheros = (int) (cantidadFicherosTemp) + 1;
				registrosPorFichero = (long) Math.ceil(totalRegistrosFichero / numeroFicheros);

			} else if (cantidadFicherosTemp >= 1 && residuo == 0) {

				numeroFicheros = (int) (cantidadFicherosTemp);

				if (totalRegistrosFichero < numeroFicheros) {

					logger.info("Entro " + numeroFicheros);

					registrosPorFichero = totalRegistrosFichero;

				} else {

					registrosPorFichero = (long) Math.ceil(totalRegistrosFichero / numeroFicheros);
				}

			}

		} catch (Exception e) {

			logger.error(mensajeTransversal + "[Error Generado:]" + e);
			
		} finally {

			logger.info(mensajeTransversal + "[FIN] - [METODO: determinarRegistroPorFichero] ");
		}

		return registrosPorFichero;

	}

	
	public String cruzarBaseConPlantilla(String trazabilidadParam,String msje, String codigoPlantilla) {

		
		this.logger.info(trazabilidadParam + "[INICIO]-[Actividad 7. Cruzar Bases con plantilla]");
		
		try {

			String cadena = propertiesExterno.cLISTA_PLANTILLA_CAMPO;
			String delimiter = Constantes.VALOR_PALOTE_UNIC;

			String array[] = cadena.split(delimiter);
						
			for (int i = 0; i < array.length; i++) {

				String separadorDosPuntos[] = array[i].split(Constantes.SIGNO_DOS_PUNTOS);
				
				if (separadorDosPuntos[0].equals(codigoPlantilla)) {
					for (int j = 0; j < separadorDosPuntos.length; j++)

					{
						
						String separadorComa[] = separadorDosPuntos[1].split(Constantes.SEPARADOR_COMA);

						for (int k = 0; k < separadorComa.length; k++) {
							
							String separadorIgual[] = separadorComa[k].split(Constantes.CARACTER_IGUAL);
							
							if (msje.contains(separadorIgual[0])) {
								
								if(!separadorIgual[0].equals(Constantes.CAMPO_NROTELEFONICO)) {
									
									msje = msje.replace(Constantes.LLAVE_IZQUIERDA+separadorIgual[0]+Constantes.LLAVE_DERECHA, Constantes.COMILLA_DOBLE + separadorIgual[1]+Constantes.COMILLA_DOBLE);

								}else {
									
									msje = msje.replace(Constantes.LLAVE_IZQUIERDA+separadorIgual[0]+Constantes.LLAVE_DERECHA, separadorIgual[1]);

									
								}
																
							}

						}

					}

				}

			}


		} catch (Exception e) {
			logger.info(e.toString());
			
		}finally {
			
			this.logger.info(trazabilidadParam + "[FIN]-[Actividad 7. Cruzar Bases con plantilla]");

		}


		return msje;
	}
	
}
