package pe.com.claro.eai.notificainfomasivo.bean.createCampaingApp.request;

public class Plantilla {

	private String titulo;
	private String descripcionPush;
	private String descripcionLargaBandeja;
	private String descripcionCortaBandeja;
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescripcionPush() {
		return descripcionPush;
	}
	public void setDescripcionPush(String descripcionPush) {
		this.descripcionPush = descripcionPush;
	}
	public String getDescripcionLargaBandeja() {
		return descripcionLargaBandeja;
	}
	public void setDescripcionLargaBandeja(String descripcionLargaBandeja) {
		this.descripcionLargaBandeja = descripcionLargaBandeja;
	}
	public String getDescripcionCortaBandeja() {
		return descripcionCortaBandeja;
	}
	public void setDescripcionCortaBandeja(String descripcionCortaBandeja) {
		this.descripcionCortaBandeja = descripcionCortaBandeja;
	}
	
	
	
}
