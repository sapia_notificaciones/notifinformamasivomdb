package pe.com.claro.eai.notificainfomasivo.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.xmlbeans.XmlObject;

public class JAXBUtilitarios {
	private static final Logger logger = LoggerFactory.getLogger(JAXBUtilitarios.class.getName());
	@SuppressWarnings("rawtypes")
	private static HashMap<Class, JAXBContext> mapContexts = new HashMap<Class, JAXBContext>();

	@SuppressWarnings("rawtypes")
	private static JAXBContext obtainJaxBContextFromClass(Class clas) {
		JAXBContext context;
		context = mapContexts.get(clas);
		if (context == null) {
			try {
				logger.info("Inicializando jaxcontext... para la clase " + clas.getName());
				context = JAXBContext.newInstance(clas);
				mapContexts.put(clas, context);
			} catch (Exception e) {
				logger.error("Error creando JAXBContext:", e);
			}
		}
		return context;
	}

	public String getXmlTextFromJaxB(Object objJaxB) {
		String commandoRequestEnXml = null;
		JAXBContext context;
		try {
			context = obtainJaxBContextFromClass(objJaxB.getClass());
			Marshaller marshaller = context.createMarshaller();
			StringWriter xmlWriter = new StringWriter();
			marshaller.marshal(objJaxB, xmlWriter);
			XmlObject xmlObj = XmlObject.Factory.parse(xmlWriter.toString());
			commandoRequestEnXml = xmlObj.toString();
		} catch (Exception e) {
			logger.error("Error parseando object to xml:", e);
		}
		return commandoRequestEnXml;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String anyObjectToXmlText(Object objJaxB) {
		String commandoRequestEnXml = null;
		if (objJaxB != null) {
			JAXBContext context;
			try {
				context = obtainJaxBContextFromClass(objJaxB.getClass());
				Marshaller marshaller = context.createMarshaller();
				StringWriter xmlWriter = new StringWriter();
				marshaller.marshal(
						new JAXBElement(new QName("", objJaxB.getClass().getName()), objJaxB.getClass(), objJaxB),
						xmlWriter);
				XmlObject xmlObj = XmlObject.Factory.parse(xmlWriter.toString());
				commandoRequestEnXml = xmlObj.toString();
			} catch (Exception e) {
				logger.error("Error parseando object to xml:", e);
			}
		} else {
			commandoRequestEnXml = "El objeto es nulo";
		}
		return commandoRequestEnXml;
	}

	public static String getTagValue(String xml, String tagName) {
		return xml.split("<" + tagName + ">")[1].split("</" + tagName + ">")[0];
	}

	public static String obtenerValorDeCadenaPorCampo(String paramCampo, String Cadena, String variableSplitPri,
			String variableSplitSec) {
		String[] listaObjetos = Cadena.split(variableSplitPri);
		String valorCadena = "";

		for (int i = 0; i < listaObjetos.length; i++) {
			String[] objeto = listaObjetos[i].split(variableSplitSec);
			if (paramCampo.equals(objeto[0])) {
				valorCadena = objeto[1];
				break;
			}
		}
		return valorCadena;
	}

	public static String leerArchivo(File archivo, String ip) throws FileNotFoundException, IOException {
		String cadena;
		String valor = null;
		FileReader f = new FileReader(archivo);
		BufferedReader b = new BufferedReader(f);
		while ((cadena = b.readLine()) != null) {
			if (cadena.contains(ip)) {
				String[] temp = cadena.split("=");
				valor = temp[1];
			}
		}
		b.close();
		return valor;
	}

	@SuppressWarnings("rawtypes")
	public static Object xmlTextToJaxB(String xmlText, Class clas) {

		JAXBContext context;
		Object object = null;

		try {
			context = obtainJaxBContextFromClass(clas);
			Unmarshaller um = context.createUnmarshaller();

			InputStream is = new ByteArrayInputStream(xmlText.getBytes(Constantes.UTF_8));

			object = um.unmarshal(is);

		} catch (Exception e) {
			logger.info(Constantes.ERROR_DETALLE + e);

		}
		return object;
	}

}