package pe.com.claro.eai.notificainfomasivo.exception;

public class WSTimeoutException extends BaseException {
	private static final long serialVersionUID = 1L;
	private String nombreWS;
	private String nombreURL;

	public WSTimeoutException(String codError, String nombreWS, String msjError, Exception objException) {
		super(codError, msjError, objException);
		this.nombreWS = nombreWS;
	}

	public WSTimeoutException(String codError, String nombreWS, String nombreURL, String msjError,
			Exception objException) {
		super(codError, msjError, objException);
		this.nombreWS = nombreWS;
		this.nombreURL = nombreURL;
	}

	public WSTimeoutException(String codError, String nombreWS, String msjError, Throwable objException) {
		super(codError, msjError, objException);
		this.nombreWS = nombreWS;
	}

	public WSTimeoutException(String msjError, Exception objException) {
		super(msjError, objException);
	}

	public WSTimeoutException(Exception objException) {
		super(objException);
	}

	public WSTimeoutException(String msjError) {
		super(msjError);
	}

	public String getNombreWS() {
		return nombreWS;
	}

	public void setNombreWS(String nombreWS) {
		this.nombreWS = nombreWS;
	}

	public String getNombreURL() {
		return nombreURL;
	}

	public void setNombreURL(String nombreURL) {
		this.nombreURL = nombreURL;
	}

}
