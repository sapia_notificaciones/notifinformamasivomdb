package pe.com.claro.eai.notificainfomasivo.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "ROWSET")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={"row" })
public class ListaMayor {

	private static final long serialVersionUID = 1L;
	  
	@XmlElement(name="ROW")
	private Fila row;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Fila getRow() {
		return row;
	}

	public void setRow(Fila row) {
		this.row = row;
	}
	 
	 
	   
}
