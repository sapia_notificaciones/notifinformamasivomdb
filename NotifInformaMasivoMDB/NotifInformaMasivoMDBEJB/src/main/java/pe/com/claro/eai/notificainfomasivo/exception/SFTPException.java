package pe.com.claro.eai.notificainfomasivo.exception;

public class SFTPException extends BaseException {

	private static final long serialVersionUID = 1L;

 
	public SFTPException(String code, String message) {
		super(code, message);
	}
}