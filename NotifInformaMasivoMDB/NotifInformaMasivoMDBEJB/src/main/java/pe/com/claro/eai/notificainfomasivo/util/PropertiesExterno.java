package pe.com.claro.eai.notificainfomasivo.util;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesExterno implements Serializable {
	private static final long serialVersionUID = 1L;

	/************************
	 * DATA_SOURCES NOTIFDB *
	 ************************/
	@Value("${bd.notifdb.name}")
	public String cNAME_NOTIFDB;
	
	@Value("${bd.notifdb.owner}")
	public String cOWNER_NOTIFDB;
	
	@Value("${bd.notifdb.jndi}")
	public String cJNDI_NOTIFDB;
	
	@Value("${bd.notifdb.conexion.timeout}")
	public Integer cLOGIN_TIMEOUT_NOTIFDB;
	
	@Value("${bd.notifdb.ejecucion.timeout}")
	public Integer cEXECUTION_TIMEOUT_NOTIFDB;
	
	@Value("${bd.notifdb.pck.notif.info}")
	public String cNOTIFDB_PCK_NOTIF_INFO;

	@Value("${bd.notifdb.sp.registrar.ini_proceso}")
	public String cNOTIFDB_SP_REGISTRAR_INI_PROCESO;
	
	@Value("${bd.notifdb.rpta.registrar.ini_proceso}")
	public String cNOTIFDB_RPTA_REGISTRAR_INI_PROCESO;
	
	@Value("${bd.notifdb.sp.registrar.error}")
	public String cNOTIFDB_SP_REGISTRAR_ERROR;
	
	@Value("${bd.notifdb.rpta.registrar.error}")
	public String cNOTIFDB_RPTA_REGISTRAR_ERROR;

	@Value("${bd.notifdb.sp.registrar.obtener_plant_config}")
	public String cNOTIFDB_SP_OBTENER_PLANT_CONFIG;
	
	@Value("${bd.notifdb.rpta.registrar.obtener_plant_config}")
	public String cNOTIFDB_RPTA_OBTENER_PLANT_CONFIG;
	
	@Value("${bd.notifdb.sp.actualizar.fin.proceso}")
	public String cNOTIFDB_SP_ACTUALIZAR_FIN_PROCESO;
	
	@Value("${bd.notifdb.rpta.actualizar.fin.proceso}")
	public String cNOTIFDB_RPTA_ACTUALIZAR_FIN_PROCESO;
	
	@Value("${bd.notifdb.sp.buscar.cliente_app}")
	public String cNOTIFDB_SP_BUSCAR_CLIENTE_APP;
	
	@Value("${bd.notifdb.rpta.buscar.cliente_app}")
	public String cNOTIFDB_RPTA_BUSCAR_CLIENTE_APP;
	
	@Value("${bd.notifdb.sp.actualizar.estado.lote}")
	public String cNOTIFDB_SP_ACTUALIZAR_ESTADO_LOTE;
	
	@Value("${bd.notifdb.rpta.actualizar.estado.lote}")
	public String cNOTIFDB_RPTA_ACTUALIZAR_ESTADO_LOTE;

	/************************ FTP *******************************/
	@Value("${carga.sftp.ip}")
	public String cCARGA_SFTP_IP;
	
	@Value("${carga.sftp.usuario}")
	public String cCARGA_SFTP_USUARIO;
	
	@Value("${carga.sftp.contrasena}")
	public String cCARGA_SFTP_CONTRASENA;
	
	@Value("${carga.sftp.puerto}")
	public int cCARGA_SFTP_PUERTO;
	
	@Value("${carga.sftp.timeout}")
	public int cCARGA_SFTP_TIMEOUT;

	@Value("${carga.sftp.tipo.canal.sesion}")
	public String cCARGA_SFTP_TIPO_CANAL_SESION;

	@Value("${carga.sftp.ruta.base}")
	public String cCARGA_SFTP_RUTA_BASE;
	
	@Value("${carga.sftp.lista.carpetas}")
	public String cCARGA_SFTP_LISTA_CARPETAS;
	
	@Value("${carga.sftp.permiso.lista.carpetas}")
	public String cCARGA_SFTP_PERMISO_LISTA_CARPETAS;
	
	@Value("${carga.sftp.max.tam.fichero.por.proc}")
	public String cCARGA_SFTP_MAX_TAM_FICHERO_POR_PROC;

	@Value("${carga.sftp.carpeta.sin.procesar}")
	public String cCARGA_SFTP_SIN_PROCESAR;
	
	@Value("${carga.sftp.carpeta.por.procesar}")
	public String cCARGA_SFTP_POR_PROCESAR;
	
	@Value("${carga.sftp.carpeta.procesados}")
	public String cCARGA_SFTP_CARPETA_FICHERO_PROCESADOS;
	
	@Value("${carga.sftp.carpeta.error}")
	public String cCARGA_SFTP_TAMANIO_FICHERO_ERROR;

	/*************** GENERALES **********************/

	@Value("${nombre.componente.registro.inicio.proceso}")
	public String cCOMPONENTE_REGISTRO_INI_PROCESO;

	/************* IDTs GENERICOS *************/
	@Value("${cod.error.tecnico.idt1}")
	public String COD_ERROR_TECNICO_IDT_1; // timeout
	
	@Value("${msj.error.tecnico.idt1}")
	public String MSJ_ERROR_TECNICO_IDT_1;
	
	@Value("${cod.error.tecnico.idt2}")
	public String COD_ERROR_TECNICO_IDT_2;// disponibilidad
	
	@Value("${msj.error.tecnico.idt2}")
	public String MSJ_ERROR_TECNICO_IDT_2;
	
	@Value("${cod.error.generico.idt3}")
	public String COD_ERROR_GENERICO_IDT_3;// error generico
	
	@Value("${msg.error.generico.idt3}")
	public String MSJ_ERROR_GENERICO_IDT_3;

	
	/************* GESTOR DE NOTIFICACIONES NGAGE *************/

	@Value("${ws.envia.sms.endpointaddress}")
	public String WS_ENVIASMS_ENDPOINTADDRESS;

	@Value("${ws.envia.sms.max.connection.timeout}")
	public String WS_ENVIASMS_MAX_CONNECTION_TIME_OUT;

	@Value("${ws.envia.sms.max.request.timeout}")
	public String WS_ENVIASMS_MAX_REQUEST_TIME_OUT;

	@Value("${ws.envia.sms.errortimeout}")
	public String WS_ENVIASMS_ERROR_TIME_OUT;

	@Value("${ws.envia.sms.codigo.idt1}")
	public String WS_ENVIASMS_CODIGO_IDT1;

	@Value("${ws.envia.sms.mensaje.idt1}")
	public String WS_ENVIASMS_MENSAJE_IDT1;

	@Value("${ws.envia.sms.codigo.idt2}")
	public String WS_ENVIASMS_CODIGO_IDT2;

	@Value("${ws.envia.sms.mensaje.idt2}")
	public String WS_ENVIASMS_MENSAJE_IDT2;

	@Value("${ws.envia.sms.tipo.envio}")
	public String WSSMSTYPESHIPPING;

	@Value("${ws.envia.sms.mensaje.remitente.user}")
	public String WSSMSREMITENTEUSER;

	@Value("${ws.envia.sms.mensaje.remitente.password}")
	public String WSREMITENTEPASSWORD;

	@Value("${ws.envia.sms.mensaje.remitente.address}")
	public String WSSMSREMITENTEADDRESS;

	@Value("${ws.envia.sms.mensaje.campaign.name}")
	public String WSSMSCAMPAIGNNAME;

	@Value("${ws.envia.sms.mensaje.campaign.desc}")
	public String WSSMSCAMPAIGNDESC;

	@Value("${ws.envia.sms.mensaje.campaign.categ}")
	public String WSSMSCAMPAIGNCATEG;

	@Value("${ws.envia.sms.mensaje.promo.categ}")
	public String WSSMSPROMOCATEG;

	@Value("${ws.envia.sms.mensaje.protocolo.id}")
	public String WSSMSPROTOCOLID;

	@Value("${ws.envia.sms.mensaje.tipo.contenido}")
	public String WSSMSCONTENTTYPE;

	@Value("${ws.envia.sms.mensaje.callback.url}")
	public String WSSMSCALLBACKURL;

	@Value("${ws.envia.sms.mensaje.scheduled.delivery.date}")
	public String WSSMSSCHEDULEDDELYDATE;

	@Value("${ws.envia.sms.mensaje.validity.period.date}")
	public String WSSMSVALIDPERIODDATE;;

	@Value("${ws.envia.sms.mensaje.delivery.report}")
	public String WSSMSDELIVERYREPORT;
	
	@Value("${ws.envia.sms.carpeta}")
	public String WSENVIOSMSCARPETA;

	@Value("${ngage.formato.fechahora}")
	public String NGAGEFORMATOFECHAHORA;

	
	/************* GENERALES2 *************/
	
	@Value("${fechaDia.formato}")
	public String FECHADIA_FORMATO;

	@Value("${canal.SMSNG}")
	public String CANAL_SMSNG;

	@Value("${codigo.internacional.peru}")
	public String CODIGO_INTERNACIONAL_PERU;
	
	@Value("${lista.por.plantilla.campo}")
	public String cLISTA_PLANTILLA_CAMPO;

	@Value("${tipo.envio.sms}")
	public String cTIPO_ENVIO_SMS;
	
	

}