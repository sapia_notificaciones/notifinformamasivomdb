package pe.com.claro.eai.notificainfomasivo.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SMS_LISTA_ROW")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlantillaSMS {

	@XmlElement(name="TCFGV_CODCANAL")	
	private String codigo;
	@XmlElement(name="TCFGV_SHORTCODE")	
	private String codigoCorto;
	@XmlElement(name="TCFGV_MENSAJE")	
	private String mensaje;
	@XmlElement(name="TCFGV_PERIOD")	
	private String period;
	@XmlElement(name="TCFGV_DELIVER")	
	private String deliver;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodigoCorto() {
		return codigoCorto;
	}
	public void setCodigoCorto(String codigoCorto) {
		this.codigoCorto = codigoCorto;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getDeliver() {
		return deliver;
	}
	public void setDeliver(String deliver) {
		this.deliver = deliver;
	}
	
	 
}
