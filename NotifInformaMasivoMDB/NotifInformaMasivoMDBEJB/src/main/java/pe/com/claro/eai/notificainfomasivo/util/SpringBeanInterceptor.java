package pe.com.claro.eai.notificainfomasivo.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;

public class SpringBeanInterceptor extends SpringBeanAutowiringInterceptor {
	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Override
	protected BeanFactoryLocator getBeanFactoryLocator(Object target) {
		logger.info("************** OBTENIENDO: [getBeanFactoryLocator] **************");
		BeanFactoryLocator objFactoryLocator = ContextSingletonBeanFactoryLocator.getInstance();
		logger.info("objFactoryLocator 'RETURN': [" + objFactoryLocator + "]");
		return objFactoryLocator;
	}
}
