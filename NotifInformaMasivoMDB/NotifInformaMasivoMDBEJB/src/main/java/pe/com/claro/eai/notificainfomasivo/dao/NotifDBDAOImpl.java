package pe.com.claro.eai.notificainfomasivo.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;

import java.sql.SQLTimeoutException;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import oracle.jdbc.OracleTypes;
import pe.com.claro.eai.notificainfomasivo.bean.ActualizarEstLoteRequest;
import pe.com.claro.eai.notificainfomasivo.bean.BeanRegistrarBaseHistorico;
import pe.com.claro.eai.notificainfomasivo.bean.Clinea;
import pe.com.claro.eai.notificainfomasivo.bean.ListaMayor;
import pe.com.claro.eai.notificainfomasivo.bean.ObtenerConfigPlantilla;
import pe.com.claro.eai.notificainfomasivo.bean.ResponseClientesApp;
import pe.com.claro.eai.notificainfomasivo.bean.Retorno;
import pe.com.claro.eai.notificainfomasivo.dao.mapper.CursorLinea;
import pe.com.claro.eai.notificainfomasivo.exception.DBException;
import pe.com.claro.eai.notificainfomasivo.exception.DBNoDisponibleException;
import pe.com.claro.eai.notificainfomasivo.exception.DBTimeoutException;
import pe.com.claro.eai.notificainfomasivo.util.CLOBToStringConverter;
import pe.com.claro.eai.notificainfomasivo.util.Constantes;
import pe.com.claro.eai.notificainfomasivo.util.JAXBUtilitarios;
import pe.com.claro.eai.notificainfomasivo.util.PropertiesExterno;

@Repository
public class NotifDBDAOImpl implements NotifDBDAO {
	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	@Qualifier("notifDS")
	private DataSource notifDS;

	@Autowired
	PropertiesExterno propertiesExterno;

	@Override
	public Retorno registrarIniProceso(String mensajeLogMet, String idTransaccion, String destinatario,
			String componente, String notificacionXML)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException {

		logger.info(mensajeLogMet + " [INI]-[METODO: registrarIniProceso - DAO] ");
		String mensajeLog = mensajeLogMet + "[registrarIniProceso]-";
		Retorno response = new Retorno();
		String owner = null;
		String paquete = null;
		String procedure = null;
		String bd = null;

		long tiempoInicio = System.currentTimeMillis();
		try {
			bd = propertiesExterno.cNAME_NOTIFDB;
			owner = propertiesExterno.cOWNER_NOTIFDB;
			paquete = propertiesExterno.cNOTIFDB_PCK_NOTIF_INFO;
			procedure = propertiesExterno.cNOTIFDB_SP_REGISTRAR_INI_PROCESO;
			notifDS.setLoginTimeout(propertiesExterno.cLOGIN_TIMEOUT_NOTIFDB);
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(notifDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner).withCatalogName(paquete).withProcedureName(procedure)
					.declareParameters(new SqlParameter("K_ID_TRANSACCION", OracleTypes.VARCHAR),
							new SqlParameter("K_DESTINATARIO", OracleTypes.VARCHAR),
							new SqlParameter("K_COMPONENTE", OracleTypes.VARCHAR),
							new SqlParameter("K_MENSAJE", OracleTypes.VARCHAR),
							new SqlOutParameter("K_IDNOTIF_DEST", OracleTypes.VARCHAR),
							new SqlOutParameter("K_COD_RESULTADO", OracleTypes.VARCHAR),
							new SqlOutParameter("K_MSJ_RESULTADO", OracleTypes.VARCHAR));
			logger.info(mensajeLog + "Consultando BD NOTIFDB, con JNDI=" + propertiesExterno.cJNDI_NOTIFDB);
			logger.info(mensajeLog + "Tiempo permitido de ejecucion=" + propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			logger.info(mensajeLog + "Ejecutando SP : " + owner + Constantes.VALOR_PUNTO + paquete
					+ Constantes.VALOR_PUNTO + procedure);
			
			logger.info(mensajeLog + " Datos de entrada:" + "[K_ID_TRANSACCION:" + idTransaccion + "]"
					+ "[K_DESTINATARIO:" + destinatario + "]" + "[K_COMPONENTE:" + componente + "]" + "[K_MENSAJE:"
					+ notificacionXML + "]");
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("K_ID_TRANSACCION", idTransaccion)
					.addValue("K_DESTINATARIO", destinatario).addValue("K_COMPONENTE", componente)
					.addValue("K_MENSAJE", notificacionXML);
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);

			logger.info(mensajeLog + " Datos del salida:");

			if (resultMap.get("K_COD_RESULTADO").toString()
					.equals(propertiesExterno.cNOTIFDB_RPTA_REGISTRAR_INI_PROCESO)) {
				if (resultMap.get("K_IDNOTIF_DEST") != null) {
					logger.info(mensajeLog + " [K_IDNOTIF_DEST]:" + resultMap.get("K_IDNOTIF_DEST").toString());
					response.setValor1(resultMap.get("K_IDNOTIF_DEST").toString());
				} else {
					response.setValor1(Constantes.CADENA_VACIA);
				}

			} else {

			}

			response.setErrorCode(resultMap.get("K_COD_RESULTADO").toString());
			response.setErrorMsg((String) resultMap.get("K_MSJ_RESULTADO"));

			logger.info(mensajeLog + " [K_COD_RESULTADO]:" + resultMap.get("K_COD_RESULTADO").toString());
			logger.info(mensajeLog + " [K_MSJ_RESULTADO]:" + resultMap.get("K_MSJ_RESULTADO").toString());

		} catch (Exception e) {
			String errorMsg = e + Constantes.CADENA_VACIA;
			logger.error(mensajeLog + " Excepcion ocurrida en la BD {" + bd + "}: {" + errorMsg + "}");
			
			registrarError(mensajeLogMet, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_BD, errorMsg);

		} finally {
			logger.info(mensajeLog + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeLogMet + " [FIN]-[METODO: registrarIniProceso - DAO] ");
		}
		return response;
	}

	@Override
	public Retorno registrarError(String mensajeLogMet, String idTransaccion, String componente, String codError,
			String msjError) {

		logger.info(mensajeLogMet + " [INI]-[METODO: registrarError - DAO] ");
		String mensajeLog = mensajeLogMet + "[registrarError]-";

		Retorno response = new Retorno();
		String owner = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;
		long tiempoInicio = System.currentTimeMillis();
		try {
			BD = propertiesExterno.cNAME_NOTIFDB;
			owner = propertiesExterno.cOWNER_NOTIFDB;
			PAQUETE = propertiesExterno.cNOTIFDB_PCK_NOTIF_INFO;
			PROCEDURE = propertiesExterno.cNOTIFDB_SP_REGISTRAR_ERROR;
			notifDS.setLoginTimeout(propertiesExterno.cLOGIN_TIMEOUT_NOTIFDB);
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(notifDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner).withCatalogName(PAQUETE).withProcedureName(PROCEDURE)
					.declareParameters(new SqlParameter("K_ID_TRANSACCION", OracleTypes.VARCHAR),
							new SqlParameter("K_COMPONENTE", OracleTypes.VARCHAR),
							new SqlParameter("K_CODERROR", OracleTypes.VARCHAR),
							new SqlParameter("K_MSJERROR", OracleTypes.VARCHAR),
							new SqlOutParameter("K_COD_RESULTADO", OracleTypes.INTEGER),
							new SqlOutParameter("K_MSJ_RESULTADO", OracleTypes.VARCHAR));
			logger.info(mensajeLog + " Consultando BD NOTIFDB, con JNDI=" + propertiesExterno.cJNDI_NOTIFDB);
			logger.info(mensajeLog + " Tiempo permitido de ejecucion=" + propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			logger.info(mensajeLog + " Ejecutando SP : " + owner + Constantes.VALOR_PUNTO + PAQUETE
					+ Constantes.VALOR_PUNTO + PROCEDURE);
			logger.info(
					mensajeLog + " Datos de entrada:" + "[K_ID_TRANSACCION:" + idTransaccion + "]" + "[K_COMPONENTE:"
							+ componente + "]" + "[K_CODERROR:" + codError + "]" + "[K_MSJERROR:" + msjError + "]");
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("K_ID_TRANSACCION", idTransaccion)
					.addValue("K_COMPONENTE", componente).addValue("K_CODERROR", codError)
					.addValue("K_MSJERROR", msjError);
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);

			response.setErrorCode(resultMap.get("K_COD_RESULTADO").toString());
			response.setErrorMsg((String) resultMap.get("K_MSJ_RESULTADO"));

			logger.info(mensajeLog + " Datos del salida:");
			logger.info(mensajeLog + " [K_COD_RESULTADO]:" + resultMap.get("K_COD_RESULTADO").toString());
			logger.info(mensajeLog + " [K_MSJ_RESULTADO]:" + resultMap.get("K_MSJ_RESULTADO").toString());

		} catch (Exception e) {
			String errorMsg = e + Constantes.CADENA_VACIA;
			logger.error(mensajeLog + " Excepcion ocurrida en la BD {" + BD + "}: {" + errorMsg + "}");

			if (errorMsg.contains(SQLTimeoutException.class.getName())) {

				logger.error(mensajeLogMet + "Error generado" + errorMsg);

			} else if (errorMsg.contains(CannotGetJdbcConnectionException.class.getName())
					|| errorMsg.contains(BadSqlGrammarException.class.getName())) {
				logger.error(mensajeLogMet + "Error generado" + errorMsg);
			} else {
				logger.error(mensajeLogMet + "Error generado" + errorMsg);
			}
						
		} finally {
			logger.info(mensajeLog + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeLogMet + "[FIN]-[METODO: registrarError - DAO] ");
		}
		return response;
	}

	@Override
	public ObtenerConfigPlantilla obtenerConfiguracionPlantilla(String mensajeLogMet, String idNotificacion,
			String fechaEjecucion) throws Exception, DBException, DBTimeoutException, DBNoDisponibleException {

		logger.info(mensajeLogMet + " [INI]-[METODO: obtenerConfiguracionPlantilla - DAO] ");
		String mensajeLog = mensajeLogMet + "[obtenerConfiguracionPlantilla]-";
		ObtenerConfigPlantilla response = new ObtenerConfigPlantilla();
		String owner = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;

		long tiempoInicio = System.currentTimeMillis();

		ListaMayor listaPlantillas = null;

		try {
			BD = propertiesExterno.cNAME_NOTIFDB;
			owner = propertiesExterno.cOWNER_NOTIFDB;
			PAQUETE = propertiesExterno.cNOTIFDB_PCK_NOTIF_INFO;
			PROCEDURE = propertiesExterno.cNOTIFDB_SP_OBTENER_PLANT_CONFIG;
			notifDS.setLoginTimeout(propertiesExterno.cLOGIN_TIMEOUT_NOTIFDB);
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(notifDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner).withCatalogName(PAQUETE).withProcedureName(PROCEDURE)
					.declareParameters(new SqlParameter("PI_NOTIFICACION", OracleTypes.VARCHAR),
							new SqlParameter("PI_FECHA_EJECUCION", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_XML_DATA", OracleTypes.CLOB, null, new CLOBToStringConverter()),
							new SqlOutParameter("PO_COD_RESULTADO", OracleTypes.INTEGER),
							new SqlOutParameter("PO_MSJ_RESULTADO", OracleTypes.VARCHAR));
			logger.info(mensajeLog + " Consultando BD NOTIFDB, con JNDI=" + propertiesExterno.cJNDI_NOTIFDB);
			logger.info(mensajeLog + " Tiempo permitido de ejecucion=" + propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			logger.info(mensajeLog + " Ejecutando SP : " + owner + Constantes.VALOR_PUNTO + PAQUETE
					+ Constantes.VALOR_PUNTO + PROCEDURE);
			logger.info(mensajeLog + " Datos de entrada:" + "[PI_NOTIFICACION:" + idNotificacion + "]"
					+ "[PI_FECHA_EJECUCION:" + fechaEjecucion + "]");

			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PI_NOTIFICACION", idNotificacion)
					.addValue("PI_FECHA_EJECUCION", fechaEjecucion);
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);

			if (resultMap.get("PO_COD_RESULTADO").toString()
					.equals(propertiesExterno.cNOTIFDB_RPTA_OBTENER_PLANT_CONFIG)) {

				logger.info(mensajeLog + " Exito:");

				if (resultMap.get("PO_XML_DATA") != null) {

					listaPlantillas = (ListaMayor) JAXBUtilitarios
							.xmlTextToJaxB(resultMap.get("PO_XML_DATA").toString(), ListaMayor.class);

					logger.info(mensajeLog + " Datos de salida:" + resultMap.get("PO_XML_DATA").toString());
					response.setListaPlantillas(listaPlantillas);

				}

			}

			response.setErrorCode(resultMap.get("PO_COD_RESULTADO").toString());
			response.setErrorMsg((String) resultMap.get("PO_MSJ_RESULTADO"));

			logger.info(mensajeLog + " Datos del salida:");

			logger.info(mensajeLog + " [PO_COD_RESULTADO]:" + resultMap.get("PO_COD_RESULTADO").toString());
			logger.info(mensajeLog + " [PO_MSJ_RESULTADO]:" + resultMap.get("PO_MSJ_RESULTADO").toString());

		} catch (Exception e) {
			String errorMsg = e + Constantes.CADENA_VACIA;
			logger.error(mensajeLog + " Excepcion ocurrida en la BD {" + BD + "}: {" + errorMsg + "}");

			registrarError(mensajeLogMet, idNotificacion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_BD, errorMsg);
			
			if (errorMsg.contains(SQLTimeoutException.class.getName())) {
				throw new DBTimeoutException(Constantes.CADENA_VACIA, BD, owner + "." + PAQUETE + "." + PROCEDURE,
						errorMsg, e);
			} else if (errorMsg.contains(CannotGetJdbcConnectionException.class.getName())
					|| errorMsg.contains(BadSqlGrammarException.class.getName())) {
				throw new DBNoDisponibleException(Constantes.CADENA_VACIA, BD, owner + "." + PAQUETE + "." + PROCEDURE,
						errorMsg, e);
			} else {
				throw new DBException(Constantes.CADENA_VACIA, BD, owner + "." + PAQUETE + "." + PROCEDURE, errorMsg,
						e);
			}
			
			
		} finally {
			logger.info(mensajeLog + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeLogMet + " [FIN]-[METODO: obtenerConfiguracionPlantilla - DAO] ");
		}
		return response;
	}

	@Override
	public Retorno actualizarFinProceso(String mensajeLogMet, String idNotiDestinatario)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException {

		logger.info(mensajeLogMet + " [INI]-[METODO: Actualizar fin Proceso - DAO] ");
		String mensajeLog = mensajeLogMet + "[ActualizarProceso]-";

		Retorno response = new Retorno();
		String owner = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;
		long tiempoInicio = System.currentTimeMillis();
		try {
			BD = propertiesExterno.cNAME_NOTIFDB;
			owner = propertiesExterno.cOWNER_NOTIFDB;
			PAQUETE = propertiesExterno.cNOTIFDB_PCK_NOTIF_INFO;
			PROCEDURE = propertiesExterno.cNOTIFDB_SP_ACTUALIZAR_FIN_PROCESO;
			notifDS.setLoginTimeout(propertiesExterno.cLOGIN_TIMEOUT_NOTIFDB);
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(notifDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner).withCatalogName(PAQUETE).withProcedureName(PROCEDURE)
					.declareParameters(new SqlParameter("PI_IDNOTIF_DEST", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_COD_RESULTADO", OracleTypes.NUMBER),
							new SqlOutParameter("PO_MSJ_RESULTADO", OracleTypes.VARCHAR));
			logger.info(mensajeLog + " Consultando BD NOTIFDB, con JNDI=" + propertiesExterno.cJNDI_NOTIFDB);
			logger.info(mensajeLog + " Tiempo permitido de ejecucion=" + propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			logger.info(mensajeLog + " Ejecutando SP : " + owner + Constantes.VALOR_PUNTO + PAQUETE
					+ Constantes.VALOR_PUNTO + PROCEDURE);
			logger.info(mensajeLog + " Datos de entrada:" + "[PI_IDNOTIF_DEST:" + idNotiDestinatario + "]");
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PI_IDNOTIF_DEST",
					idNotiDestinatario);
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);

			response.setErrorCode(resultMap.get("PO_COD_RESULTADO").toString());
			response.setErrorMsg((String) resultMap.get("PO_MSJ_RESULTADO"));

			logger.info(mensajeLog + " Datos del salida:");
			logger.info(mensajeLog + " [PO_COD_RESULTADO]:" + resultMap.get("PO_COD_RESULTADO").toString());
			logger.info(mensajeLog + " [PO_MSJ_RESULTADO]:" + resultMap.get("PO_MSJ_RESULTADO").toString());

		} catch (Exception e) {
			String errorMsg = e + Constantes.CADENA_VACIA;
			logger.error(mensajeLog + " Excepcion ocurrida en la BD {" + BD + "}: {" + errorMsg + "}");

			registrarError(mensajeLogMet, idNotiDestinatario, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_BD, errorMsg);
			
			if (errorMsg.contains(SQLTimeoutException.class.getName())) {
				throw new DBTimeoutException(Constantes.CADENA_VACIA, BD, owner + "." + PAQUETE + "." + PROCEDURE,
						errorMsg, e);
			} else if (errorMsg.contains(CannotGetJdbcConnectionException.class.getName())
					|| errorMsg.contains(BadSqlGrammarException.class.getName())) {
				throw new DBNoDisponibleException(Constantes.CADENA_VACIA, BD, owner + "." + PAQUETE + "." + PROCEDURE,
						errorMsg, e);
			} else {
				throw new DBException(Constantes.CADENA_VACIA, BD, owner + "." + PAQUETE + "." + PROCEDURE, errorMsg,
						e);
			}
		} finally {
			logger.info(mensajeLog + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeLogMet + "[FIN]-[METODO: ActualizarProceso - DAO] ");
		}
		return response;
	}

	@Override
	public ResponseClientesApp buscarClientePorApp(String mensajeLogMet, String idNotificacion)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException {

		logger.info(mensajeLogMet + " [INI]-[METODO: Buscar cliente por APP - DAO] ");
		String mensajeLog = mensajeLogMet + "[BuscarCliente]-";

		ResponseClientesApp response = new ResponseClientesApp();
		List<Clinea> listaClientesApp = new ArrayList<Clinea>();
		String owner = null;
		String paquete = null;
		String procedure = null;
		String bd = null;
		long tiempoInicio = System.currentTimeMillis();
		try {
			bd = propertiesExterno.cNAME_NOTIFDB;
			owner = propertiesExterno.cOWNER_NOTIFDB;
			paquete = propertiesExterno.cNOTIFDB_PCK_NOTIF_INFO;
			procedure = propertiesExterno.cNOTIFDB_SP_BUSCAR_CLIENTE_APP;
			notifDS.setLoginTimeout(propertiesExterno.cLOGIN_TIMEOUT_NOTIFDB);
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(notifDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner).withCatalogName(paquete).withProcedureName(procedure)
					.declareParameters(new SqlParameter("PI_NOTIFICACION", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_LINEAS", OracleTypes.CURSOR, new CursorLinea()),
							new SqlOutParameter("PO_COD_RESULTADO", OracleTypes.INTEGER),
							new SqlOutParameter("PO_MSJ_RESULTADO", OracleTypes.VARCHAR));
			logger.info(mensajeLog + " Consultando BD NOTIFDB, con JNDI=" + propertiesExterno.cJNDI_NOTIFDB);
			logger.info(mensajeLog + " Tiempo permitido de ejecucion=" + propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			logger.info(mensajeLog + " Ejecutando SP : " + owner + Constantes.VALOR_PUNTO + paquete
					+ Constantes.VALOR_PUNTO + procedure);
			logger.info(mensajeLog + " Datos de entrada:" + "[PI_NOTIFICACION:" + idNotificacion + "]");
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PI_NOTIFICACION",
					idNotificacion);
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);

			response.setErrorCode(resultMap.get("PO_COD_RESULTADO").toString());
			response.setErrorMsg((String) resultMap.get("PO_MSJ_RESULTADO"));

			listaClientesApp = (List<Clinea>) resultMap.get("PO_LINEAS");

			response.setListClientesApp(listaClientesApp);

			logger.info(mensajeLog + " Datos del salida:");
			logger.info(mensajeLog + " [PO_COD_RESULTADO]:" + resultMap.get("PO_COD_RESULTADO").toString());
			logger.info(mensajeLog + " [PO_MSJ_RESULTADO]:" + resultMap.get("PO_MSJ_RESULTADO").toString());
			logger.info(mensajeLog + " [PO_LINEAS]:" + listaClientesApp);

		} catch (Exception e) {
			String errorMsg = e + Constantes.CADENA_VACIA;
			logger.error(mensajeLog + " Excepcion ocurrida en la BD {" + bd + "}: {" + errorMsg + "}");

			registrarError(mensajeLogMet, idNotificacion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_BD, errorMsg);
			
			if (errorMsg.contains(SQLTimeoutException.class.getName())) {
				throw new DBTimeoutException(Constantes.CADENA_VACIA, bd, owner + "." + paquete + "." + procedure,
						errorMsg, e);
			} else if (errorMsg.contains(CannotGetJdbcConnectionException.class.getName())
					|| errorMsg.contains(BadSqlGrammarException.class.getName())) {
				throw new DBNoDisponibleException(Constantes.CADENA_VACIA, bd, owner + "." + paquete + "." + procedure,
						errorMsg, e);
			} else {
				throw new DBException(Constantes.CADENA_VACIA, bd, owner + "." + paquete + "." + procedure, errorMsg,
						e);
			}
		} finally {
			logger.info(mensajeLog + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeLogMet + "[FIN]-[METODO: BuscarCliente - DAO] ");
		}
		return response;
	}

	@Override
	public Retorno insertCargaHistorico(String mensajeLogMet,String idTransaccion, List<BeanRegistrarBaseHistorico> lista) {
		Retorno retorno = new Retorno();
		long tiempoInicio = System.currentTimeMillis();
		logger.info(mensajeLogMet + "[INICIO]-[METODO: RegistroHistorico - DAO] ");
		try {
			Connection conn = notifDS.getConnection();

			if (conn != null) {

				String sql = "INSERT INTO " + Constantes.TABLA_NOTIFICA_REGISTRO + "(" + Constantes.CAMPO_IDNOTIFICACION
						+ "," + Constantes.CAMPO_IDENTIFICADOR + "," + Constantes.CAMPO_CANAL + ","
						+ Constantes.CAMPO_LOTE + "," + Constantes.CAMPO_TIPO + "," + Constantes.CAMPO_ESTADO + ","
						+ Constantes.CAMPO_FECHA + " )"

						+ " VALUES (" + Constantes.CARACTER_INTERROGANTE + "," + Constantes.CARACTER_INTERROGANTE + ","
						+ Constantes.CARACTER_INTERROGANTE + "," + Constantes.CARACTER_INTERROGANTE + ","
						+ Constantes.CARACTER_INTERROGANTE + "," + Constantes.CARACTER_INTERROGANTE + ", to_date("
						+ Constantes.CARACTER_INTERROGANTE + "," + Constantes.COMILLA_SIMPLE+propertiesExterno.FECHADIA_FORMATO+Constantes.COMILLA_SIMPLE+ "))";

				PreparedStatement pstmt = conn.prepareStatement(sql);

				for (BeanRegistrarBaseHistorico params : lista) {

					pstmt.setString(1, params.getIdNotificacion());
					pstmt.setString(2, params.getIdentificador());
					pstmt.setString(3, params.getCanal());
					pstmt.setInt(4, params.getLote());
					pstmt.setString(5, params.getTipo());
					pstmt.setInt(6, params.getEstado());
					pstmt.setString(7, params.getFecha());

					pstmt.addBatch();

				}

				pstmt.executeBatch();

				conn.commit();
				conn.close();

				logger.info("Se ejecuto correctamente la insercion a la tabla historica");

				retorno.setErrorCode(Constantes.VALOR_CERO);
				retorno.setErrorMsg(Constantes.MENSAJE_OK);

			} else {
				logger.info(mensajeLogMet + Constantes.MENSAJE_CONEXION_ERRONEA);
				logger.info(mensajeLogMet + "No se ejecuto correctamente la insercion a la tabla historica");
			}

		} catch (Exception e) {

			logger.info(e.toString());
			
			registrarError(mensajeLogMet, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_BD, e.toString());

		} finally {
			logger.info(mensajeLogMet + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeLogMet + "[FIN]-[METODO: RegistroHistorico - DAO] ");

		}
		return retorno;

	}

	@Override
	public Retorno actualizarEstadoLote(String mensajeLogMet, String idTransaccion,ActualizarEstLoteRequest request)
			throws Exception, DBException, DBTimeoutException, DBNoDisponibleException {

		logger.info(mensajeLogMet + " [INI]-[METODO: Actualizar estadoLote - DAO] ");
		String mensajeLog = mensajeLogMet + "[ActualizarEstadoLote]-";

		Retorno response = new Retorno();
		String OWNER = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;
		long tiempoInicio = System.currentTimeMillis();
		try {
			BD = propertiesExterno.cNAME_NOTIFDB;
			OWNER = propertiesExterno.cOWNER_NOTIFDB;
			PAQUETE = propertiesExterno.cNOTIFDB_PCK_NOTIF_INFO;
			PROCEDURE = propertiesExterno.cNOTIFDB_SP_ACTUALIZAR_ESTADO_LOTE;
			notifDS.setLoginTimeout(propertiesExterno.cLOGIN_TIMEOUT_NOTIFDB);
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(notifDS).withoutProcedureColumnMetaDataAccess()
					.withSchemaName(OWNER).withCatalogName(PAQUETE).withProcedureName(PROCEDURE)
					.declareParameters(new SqlParameter("PI_LOTE", OracleTypes.INTEGER),
							new SqlParameter("PI_NOTIFICACION", OracleTypes.VARCHAR),
							new SqlParameter("PI_CANAL", OracleTypes.VARCHAR),
							new SqlParameter("PI_ESTADO", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_COD_RESULTADO", OracleTypes.NUMBER),
							new SqlOutParameter("PO_MSJ_RESULTADO", OracleTypes.VARCHAR));
			logger.info(mensajeLog + " Consultando BD NOTIFDB, con JNDI=" + propertiesExterno.cJNDI_NOTIFDB);
			logger.info(mensajeLog + " Tiempo permitido de ejecucion=" + propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			jdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.cEXECUTION_TIMEOUT_NOTIFDB);
			logger.info(mensajeLog + " Ejecutando SP : " + OWNER + Constantes.VALOR_PUNTO + PAQUETE
					+ Constantes.VALOR_PUNTO + PROCEDURE);
			logger.info(mensajeLog + " Datos de entrada:" + "[PI_LOTE:" + request.getLote() + "]");
			logger.info(mensajeLog + " Datos de entrada:" + "[PI_NOTIFICACION:" + request.getIdNotificacion() + "]");
			logger.info(mensajeLog + " Datos de entrada:" + "[PI_CANAL:" + request.getCanal() + "]");
			logger.info(mensajeLog + " Datos de entrada:" + "[PI_ESTADO:" + request.getEstado() + "]");
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PI_LOTE", request.getLote())
					.addValue("PI_NOTIFICACION", request.getIdNotificacion()).addValue("PI_CANAL", request.getCanal())
					.addValue("PI_ESTADO", request.getEstado());
			Map<String, Object> resultMap = jdbcCall.execute(objParametrosIN);

			response.setErrorCode(resultMap.get("PO_COD_RESULTADO").toString());
			response.setErrorMsg((String) resultMap.get("PO_MSJ_RESULTADO"));

			logger.info(mensajeLog + " Datos del salida:");
			logger.info(mensajeLog + " [PO_COD_RESULTADO]:" + resultMap.get("PO_COD_RESULTADO").toString());
			logger.info(mensajeLog + " [PO_MSJ_RESULTADO]:" + resultMap.get("PO_MSJ_RESULTADO").toString());

		} catch (Exception e) {
			String errorMsg = e + Constantes.CADENA_VACIA;
			logger.error(mensajeLog + " Excepcion ocurrida en la BD {" + BD + "}: {" + errorMsg + "}");

			registrarError(mensajeLogMet, idTransaccion, Constantes.COMPONENTE_MDB_NOTIF_MASIVO, Constantes.CODIGO_ERROR_BD, errorMsg);
			
			if (errorMsg.contains(SQLTimeoutException.class.getName())) {
				throw new DBTimeoutException(Constantes.CADENA_VACIA, BD, OWNER + "." + PAQUETE + "." + PROCEDURE,
						errorMsg, e);
			} else if (errorMsg.contains(CannotGetJdbcConnectionException.class.getName())
					|| errorMsg.contains(BadSqlGrammarException.class.getName())) {
				throw new DBNoDisponibleException(Constantes.CADENA_VACIA, BD, OWNER + "." + PAQUETE + "." + PROCEDURE,
						errorMsg, e);
			} else {
				throw new DBException(Constantes.CADENA_VACIA, BD, OWNER + "." + PAQUETE + "." + PROCEDURE, errorMsg,
						e);
			}
		} finally {
			logger.info(mensajeLog + " Tiempo TOTAL Proceso: [" + (tiempoInicio - System.currentTimeMillis())
					+ " milisegundos ]");
			logger.info(mensajeLogMet + "[FIN]-[METODO: ActualizarProceso - DAO] ");
		}
		return response;
	}

}
